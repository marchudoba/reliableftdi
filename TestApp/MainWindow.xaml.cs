﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FtdiWrapper;
using GalaSoft.MvvmLight.Messaging;
using TestApp.ViewModel;

namespace TestApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Messenger.Default.Register<NotificationMessageAction<List<DeviceInfo>[]>>(this, ProcessMessages);
            Messenger.Default.Register<NotificationMessage>(this, ProcessMessages);

            Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Messenger.Default.Send(new NotificationMessage(MessageLabels.Exiting));

            if(LogWindow != null)
            {
                LogWindow.Close();
            }
        }

        private Window LogWindow;

        private void ProcessMessages(NotificationMessage m)
        {
            if (m.Notification == MessageLabels.OpenLog)
            {
                if (LogWindow == null)
                {
                    LogWindow = new LogView();
                    LogWindow.Show();
                    LogWindow.Closed += LogWindow_Closed;
                }
            }
            if(m.Notification == MessageLabels.OpenManual)
            {
                ManualWindow manual = new ManualWindow();
                manual.ShowDialog();
            }
        }

        private void LogWindow_Closed(object sender, EventArgs e)
        {
            LogWindow.Closed -= LogWindow_Closed;
            LogWindow = null;
        }

        private void ProcessMessages(NotificationMessageAction<List<DeviceInfo>[]> m)
        {
            if (m.Notification == MessageLabels.AssociateDevices)
            {
                Window w = new AssociateDevicesWindow();
                bool? res = w.ShowDialog();

                if (res.HasValue && res.Value)
                {
                    List<DeviceInfo>[] ret = new List<DeviceInfo>[2];
                    ret[0] = ((w as AssociateDevicesWindow).DataContext as AssociateDevicesViewModel).Protocol1Devices;
                    ret[1] = ((w as AssociateDevicesWindow).DataContext as AssociateDevicesViewModel).Protocol2Devices;

                    m.Execute(ret);
                }
            }
        }
    }
}
