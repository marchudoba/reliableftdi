﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using FtdiWrapper.Logging;

namespace TestApp
{
    class LogEntryTemplateSelector : DataTemplateSelector
    {
        public DataTemplate ExceptionLogEntryTemplate { get; set; }
        public DataTemplate ObjectLogEntryTemplate { get; set; }
        public DataTemplate TextLogEntryTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if(item is TextLogEntry)
            {
                return TextLogEntryTemplate;
            }
            else if(item is ExceptionLogEntry)
            {
                return ExceptionLogEntryTemplate;
            }
            else if(item is ObjectLogEntry)
            {
                return ObjectLogEntryTemplate;
            }

            return base.SelectTemplate(item, container);
        }
    }
}
