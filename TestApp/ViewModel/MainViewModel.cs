using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using FtdiWrapper;
using FtdiWrapper.Protocols;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using System.Linq;
using System;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;
using GalaSoft.MvvmLight.Threading;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using FtdiWrapper.Logging;

namespace TestApp.ViewModel
{
    public enum Protocols { Protocol1, Protocol2 }

    public class MessageOptions : ViewModelValidatorBase
    {
        public MessageOptions()
        {
            // content cant be empty when non random
            AddRule(new DelegateRule(() =>
            {
                if (Random == false)
                {
                    return string.IsNullOrWhiteSpace(Content) == false;
                }

                return true;
            }, "Content can't empty.", nameof(Content)));

            //lenght has to be number
            AddRule(new DelegateRule(() =>
            {
                if (Random)
                {
                    int dum;
                    if (int.TryParse(MinLength, out dum) == false)
                    {
                        return false;
                    }
                    if (int.TryParse(MaxLength, out dum) == false)
                    {
                        return false;
                    }
                }

                return true;
            }, "Length has to be number.", nameof(MinLength), nameof(MaxLength)));

            //interval has to be number
            AddRule(new DelegateRule(() =>
            {
                if (Random && SendPeriodicaly)
                {
                    int dum;
                    if (int.TryParse(Interval, out dum) == false)
                    {
                        return false;
                    }
                }

                return true;
            }, "Interval has to be number.", nameof(Interval)));

            //lenght have to be positive
            AddRule(new DelegateRule(() =>
            {
                if (Random && GetMinLength() <= 0)
                {
                    return false;
                }
                if (Random && GetMaxLength() <= 0)
                {
                    return false;
                }

                return true;
            }, "Lenght has to be posive.", nameof(MinLength), nameof(MaxLength)));

            // min lenght cant be smaller than max length
            AddRule(new DelegateRule(() =>
            {
                if (Random && GetMinLength() > GetMaxLength())
                {
                    return false;
                }

                return true;
            }, "Min length can't smaller max length.", nameof(MinLength), nameof(MaxLength)));

            // interval cant be empty
            AddRule(new DelegateRule(() =>
            {
                if (SendPeriodicaly)
                {
                    return GetInterval() < 0 == false;
                }

                return true;
            }, "Interval can't be negative.", nameof(Interval)));

            //hex has to contain only valid characters or whitespace
            AddRule(new DelegateRule(() =>
            {
                if (Content == null)
                {
                    return true;
                }

                if (SelectedContentType == ContentType.Hex)
                {
                    return Regex.IsMatch(Content, "[^1234567890ABCDEFabcdef ]") == false;
                }

                return true;
            }, "Content contains invalid characters. Input only hexadecimal digits or space.", nameof(Content)));

            //hex has to be byte aligned
            AddRule(new DelegateRule(() =>
            {
                if (Content == null)
                {
                    return true;
                }

                if (SelectedContentType == ContentType.Hex)
                {
                    var dataNoWS = new string(Content.Where(x => char.IsWhiteSpace(x) == false).ToArray());

                    if (dataNoWS.Length % 2 != 0)
                    {
                        return false;
                    }
                }

                return true;
            }, "Content is not byte aligned. Content has have even length.", nameof(Content)));
        }

        public enum ContentType { String, Hex }

        public ContentType SelectedContentType { get; set; } = ContentType.String;

        public bool Random { get; set; } = false;
        public bool SendPeriodicaly { get; set; } = false;

        public string MinLength { get; set; } = "10";
        public string MaxLength { get; set; } = "100";
        public string Interval { get; set; } = "500";
        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                Set(ref content, value);
            }
        }

        private string content;

        public int GetMinLength()
        {
            int ret;
            if (int.TryParse(MinLength, out ret))
            {
                return ret;
            }

            return 0;
        }
        public int GetMaxLength()
        {
            int ret;
            if (int.TryParse(MaxLength, out ret))
            {
                return ret;
            }

            return 0;
        }
        public int GetInterval()
        {
            int ret;
            if (int.TryParse(Interval, out ret))
            {
                return ret;
            }

            return 0;
        }
        public bool ValidateAll()
        {
            return Validate(nameof(Content)) && Validate(nameof(MinLength)) && Validate(nameof(MaxLength)) && Validate(nameof(Interval));
        }
    }

    class Message
    {
        public static int MaxLength = 255;

        public Message()
        {

        }
        public Message(MessageOptions.ContentType type, byte[] data)
        {
            if (data.Length > MaxLength)
            {
                throw new ArgumentException("Data is too long.");
            }

            Type = type;
            Data = data;
        }

        public MessageOptions.ContentType Type { get; private set; }
        public byte[] Data { get; private set; }

        public bool CanDeserialize(List<byte> data)
        {
            if (data == null)
            {
                return false;
            }
            if (data.Count < 2)
            {
                return false;
            }

            MessageOptions.ContentType type = (MessageOptions.ContentType)data[0];
            int length = (int)data[1];

            if (data.Count < length + 2)
            {
                return false;
            }

            return true;
        }
        public void Deserialize(List<byte> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            if (data.Count < 2)
            {
                throw new ArgumentException("Data too short.");
            }

            MessageOptions.ContentType type = (MessageOptions.ContentType)data[0];
            int length = (int)data[1];

            if (data.Count < length + 2)
            {
                throw new ArgumentException("Data too short.");
            }

            Type = type;
            Data = new byte[length];
            data.CopyTo(2, Data, 0, length);
            data.RemoveRange(0, length + 2);
        }
        public byte[] Serialize()
        {
            var data = Data as byte[];
            byte[] ret = new byte[2 + data.Length];

            ret[0] = (byte)Type;
            ret[1] = (byte)data.Length;
            Array.Copy(data, 0, ret, 2, data.Length);
            return ret;
        }
    }

    // put receivetask cancelation into seperate method and call in exceptions
    public class MainViewModel : ViewModelValidatorBase
    {

        class ProtocolInfo
        {
            public ProtocolBundle Protocol { get; set; }
            public List<DeviceInfo> Devices { get; private set; } = new List<DeviceInfo>();

            public string Label { get; set; }
            public bool IsOpening { get; set; } = false;
            public bool IsClosing { get; set; } = false;
            public bool IsSending { get; set; } = false;
            public bool IsSendingPeriodicaly { get; set; } = false;

            public Task SendPeriodicalyTask { get; set; }
            public Task ReceiveTask { get; set; }
            public CancellationTokenSource CancelReceiveTask { get; set; }
            public CancellationTokenSource CancelSendPeriodicalyTask { get; set; }
            public List<byte> Buffer { get; private set; } = new List<byte>();

        }

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            AssociateDevices = new RelayCommand(AssociateDevicesExecute);

            Open = new RelayCommand(OpenExecute, OpenCanExecute);
            Listen = new RelayCommand(ListenExecute, ListenCanExecute);
            Close = new RelayCommand(CloseExecute, CloseCanExcecute);
            Send = new RelayCommand(SendExecute, SendCanExecute);
            StopSendPeriodicaly = new RelayCommand(StopSendPeriodicalyExecute, StopSendPeriodicalyCanExecute);
            OpenLog = new RelayCommand(OpenLogExecute);
            LogToFile = new RelayCommand(LogToFileExecute);
            OpenManual = new RelayCommand(OpenManualExecute);
        }

        private Protocols selectedProtocol = Protocols.Protocol1;

        public MessageOptions MessageOptions { get; private set; } = new MessageOptions();
        public ObservableCollection<string> Output { get; private set; } = new ObservableCollection<string>();
        public Protocols SelectedProtocol
        {
            get
            {
                return selectedProtocol;
            }
            set
            {
                selectedProtocol = value;
                Listen.RaiseCanExecuteChanged();
                Close.RaiseCanExecuteChanged();
                Open.RaiseCanExecuteChanged();
                Send.RaiseCanExecuteChanged();
                StopSendPeriodicaly.RaiseCanExecuteChanged();
                RaisePropertyChanged(nameof(SelectedProtocolIsSendingPeriodicaly));
            }
        }
        public bool SelectedProtocolIsSendingPeriodicaly
        {
            get
            {
                if (SelectedProtocol == Protocols.Protocol1)
                {
                    return protocol1.IsSendingPeriodicaly;
                }
                else if (SelectedProtocol == Protocols.Protocol2)
                {
                    return protocol2.IsSendingPeriodicaly;
                }
                return false;
            }
        }
        public ICommand AssociateDevices { get; private set; }
        public RelayCommand Open { get; private set; }
        public RelayCommand Listen { get; private set; }
        public RelayCommand Close { get; private set; }
        public RelayCommand Send { get; private set; }
        public RelayCommand StopSendPeriodicaly { get; private set; }
        public RelayCommand OpenLog { get; private set; }
        public RelayCommand LogToFile { get; private set; }
        public RelayCommand OpenManual { get; private set; }

        private ProtocolInfo protocol1 = new ProtocolInfo { Label = "Protocol1: " };
        private ProtocolInfo protocol2 = new ProtocolInfo { Label = "Protocol2: " };

        private CancellationTokenSource logCts;

        private Random random = new Random();

        private void LogToFileExecute()
        {
            if (logCts != null)
            {
                return;
            }

            string logName = "";
            Messenger.Default.Send(new NotificationMessageAction<string>(MessageLabels.OpenFileNameDialog, (name) => logName = name));

            if (logName == "")
            {
                return;
            }

            Task.Run(() =>
            {
                Logger.Instance.ExclusionFilter.Add(Logger.Id.NativeCall);
                Logger.Instance.ExclusionFilter.Add(Logger.Id.DeviceStream);
                ConcurrentQueue<LogEntry> log = new ConcurrentQueue<LogEntry>();
                logCts = new CancellationTokenSource();

                Logger.Instance.NewEntry += (s, e) => log.Enqueue(e.NewEntry);
                using (var logFile = File.Open(logName, FileMode.OpenOrCreate))
                {
                    LogEntry e;

                    while (logCts.IsCancellationRequested == false)
                    {
                        while (log.TryDequeue(out e) && logCts.IsCancellationRequested == false)
                        {
                            var data = Encoding.ASCII.GetBytes(e.ToString() + Environment.NewLine);
                            logFile.Write(data, 0, data.Length);
                            logFile.Flush();
                        }
                        Task.Delay(100).Wait();
                    }


                    while (log.TryDequeue(out e))
                    {
                        var data = Encoding.ASCII.GetBytes(e.ToString() + Environment.NewLine);
                        logFile.Write(data, 0, data.Length);
                    }
                    logFile.Flush();
                }
            }).ConfigureAwait(false);
        }

        private void OpenLogExecute()
        {
            Messenger.Default.Send(new NotificationMessage(MessageLabels.OpenLog));
        }
        private bool OpenCanExecute()
        {
            if (SelectedProtocol == Protocols.Protocol1 && protocol1.IsOpening == false)
            {
                return OpenCanExecute(protocol1);
            }
            else if (SelectedProtocol == Protocols.Protocol2 && protocol2.IsOpening == false)
            {
                return OpenCanExecute(protocol2);
            }

            return false;
        }
        private bool OpenCanExecute(ProtocolInfo pi)
        {
            if (pi.Devices.Count == 0)
            {
                return false;
            }
            if (pi.Protocol == null)
            {
                return true;
            }

            return pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Opening;
        }
        private async void OpenExecute()
        {
            if (SelectedProtocol == Protocols.Protocol1)
            {
                await OpenExecute(protocol1);
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                await OpenExecute(protocol2);
            }
        }
        private async Task OpenExecute(ProtocolInfo pi)
        {
            lock (pi)
            {
                if (pi.IsOpening)
                {
                    return;
                }

                pi.IsOpening = true;
            }

            Open.RaiseCanExecuteChanged();
            if (pi.Protocol == null)
            {
                AddOutput(pi.Label + "Creating protocol bundle from " + string.Concat(pi.Devices.Select(x => x.LocationId.ToString() + ",")).TrimEnd(',') + ".");
                pi.Protocol = new ProtocolBundle(pi.Devices.ToArray());
            }

            await Task.Run(() =>
            {
                try
                {
                    for (int i = 0; i < 5; i++)
                    {
                        AddOutput(pi.Label + "Attemting to open. Attempt " + (i + 1) + "/5.");

                        if (pi.Protocol.Open(1000))
                        {
                            pi.CancelReceiveTask = new CancellationTokenSource();
                            pi.ReceiveTask = Task.Run(() => ReceiveTask(pi), pi.CancelReceiveTask.Token);
                            AddOutput(pi.Label + "Open succesfull.");
                            return;
                        }
                    }
                }
                catch (IOException e)
                {
                    AddOutput(pi.Label + e.ToString());

                    CleanupProtocol(pi);
                }
                catch (Exception)
                {

                }
                AddOutput(pi.Label + "Failed to open.");
            });

            lock (pi)
            {
                pi.IsOpening = false;
            }
            Listen.RaiseCanExecuteChanged();
            Close.RaiseCanExecuteChanged();
            Open.RaiseCanExecuteChanged();
            Send.RaiseCanExecuteChanged();
        }

        private bool ListenCanExecute()
        {
            return OpenCanExecute();
        }
        private void ListenExecute()
        {
            if (SelectedProtocol == Protocols.Protocol1)
            {
                ListenExecute(protocol1);
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                ListenExecute(protocol2);
            }
        }
        private void ListenExecute(ProtocolInfo pi)
        {
            lock (pi)
            {
                if (pi.IsOpening)
                {
                    return;
                }

                pi.IsOpening = true;
            }

            Listen.RaiseCanExecuteChanged();
            if (pi.Protocol == null)
            {
                AddOutput(pi.Label + "Creating protocol bundle from " + string.Concat(pi.Devices.Select(x => x.LocationId.ToString() + ",")).TrimEnd(',') + ".");
                pi.Protocol = new ProtocolBundle(pi.Devices.ToArray());
            }

            try
            {
                pi.Protocol.Listen();
                pi.CancelReceiveTask = new CancellationTokenSource();
                pi.ReceiveTask = Task.Run(() => ReceiveTask(pi), pi.CancelReceiveTask.Token);
                AddOutput(pi.Label + "Listening.");
            }
            catch (IOException)
            {
                AddOutput("Failed to listen.");
                CleanupProtocol(pi);
            }
            finally
            {
                lock (pi)
                {
                    pi.IsOpening = false;
                }
                Listen.RaiseCanExecuteChanged();
                Close.RaiseCanExecuteChanged();
                Open.RaiseCanExecuteChanged();
                Send.RaiseCanExecuteChanged();
            }
        }

        private bool CloseCanExcecute()
        {
            if (SelectedProtocol == Protocols.Protocol1 && protocol1.IsClosing == false)
            {
                return CloseCanExcecute(protocol1);
            }
            else if (SelectedProtocol == Protocols.Protocol2 && protocol2.IsClosing == false)
            {
                return CloseCanExcecute(protocol2);
            }

            return false;
        }
        private bool CloseCanExcecute(ProtocolInfo pi)
        {
            if (pi.Protocol == null)
            {
                return false;
            }

            return pi.Protocol.Status != LineProtocol.LineStatus.Closed && pi.Protocol.Status != LineProtocol.LineStatus.Faulted;
        }
        private void CloseExecute()
        {
            if (SelectedProtocol == Protocols.Protocol1)
            {
                CloseExecute(protocol1);
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                CloseExecute(protocol2);
            }
        }
        private void CloseExecute(ProtocolInfo pi)
        {
            // stop periodical task
            // handle stop on the otherside
            lock (pi)
            {
                if (pi.IsClosing)
                {
                    return;
                }

                pi.IsClosing = true;
            }

            Close.RaiseCanExecuteChanged();

            //if (pi.IsSendingPeriodicaly)
            //{
            //    pi.CancelSendPeriodicalyTask.Cancel();
            //    pi.SendPeriodicalyTask.Wait();
            //    pi.SendPeriodicalyTask = null;
            //}

            //pi.Protocol.Close();


            CleanupProtocol(pi);
            AddOutput(pi.Label + "Closed.");


            lock (pi)
            {
                pi.IsClosing = false;
            }

            Listen.RaiseCanExecuteChanged();
            Close.RaiseCanExecuteChanged();
            Open.RaiseCanExecuteChanged();
            Send.RaiseCanExecuteChanged();
        }

        private bool StopSendPeriodicalyCanExecute()
        {
            if (SelectedProtocol == Protocols.Protocol1 && protocol1.IsSendingPeriodicaly == true)
            {
                return true;
            }
            else if (SelectedProtocol == Protocols.Protocol2 && protocol2.IsSendingPeriodicaly == true)
            {
                return true;
            }

            return false;
        }
        private void StopSendPeriodicalyExecute()
        {
            if (SelectedProtocol == Protocols.Protocol1)
            {
                StopSendPeriodicalyExecute(protocol1);
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                StopSendPeriodicalyExecute(protocol2);
            }
        }
        private void StopSendPeriodicalyExecute(ProtocolInfo pi)
        {
            if (pi.SendPeriodicalyTask != null)
            {
                pi.CancelSendPeriodicalyTask.Cancel();

                try
                {
                    pi.SendPeriodicalyTask.Wait();
                }
                catch (Exception e)
                {

                }

                pi.SendPeriodicalyTask = null;

                pi.IsSendingPeriodicaly = false;

                RaisePropertyChanged(nameof(SelectedProtocolIsSendingPeriodicaly));
            }
        }

        private bool SendCanExecute()
        {
            if (SelectedProtocol == Protocols.Protocol1 && protocol1.Protocol != null && protocol1.Protocol.Status == LineProtocol.LineStatus.Open && protocol1.IsSending == false)
            {
                return true;
            }
            if (SelectedProtocol == Protocols.Protocol2 && protocol2.Protocol != null && protocol2.Protocol.Status == LineProtocol.LineStatus.Open && protocol2.IsSending == false)
            {
                return true;
            }
            return false;
        }
        private void SendExecute()
        {
            if (MessageOptions.ValidateAll() == false)
            {
                return;
            }

            if (SelectedProtocol == Protocols.Protocol1)
            {
                SendExecute(protocol1);
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                SendExecute(protocol2);
            }
        }
        private async void SendExecute(ProtocolInfo pi)
        {
            lock (pi)
            {
                if (pi.IsSending)
                {
                    return;
                }

                pi.IsSending = true;
            }



            if (MessageOptions.SendPeriodicaly)
            {
                var interval = MessageOptions.Interval;
                pi.CancelSendPeriodicalyTask = new CancellationTokenSource();
                pi.SendPeriodicalyTask = Task.Run(async () =>
                {
                    Random randomGen = new Random();
                    pi.IsSendingPeriodicaly = true;
                    StopSendPeriodicaly.RaiseCanExecuteChanged();
                    RaisePropertyChanged(nameof(SelectedProtocolIsSendingPeriodicaly));

                    while (pi.CancelSendPeriodicalyTask.IsCancellationRequested == false)
                    {
                        SendMessage(pi, randomGen);
                        await Task.Delay(MessageOptions.GetInterval());
                    }

                }, pi.CancelSendPeriodicalyTask.Token);

                try
                {
                    await pi.SendPeriodicalyTask.ConfigureAwait(false);
                }
                catch (IOException e)
                {
                    if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Closing)
                    {
                        AddOutput(pi.Label + "Closed.");
                    }
                    else
                    {
                        AddOutput(pi.Label + e.ToString());
                    }

                    CleanupProtocol(pi);
                }
                catch (Exception e)
                {
                    if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Closing)
                    {
                        AddOutput(pi.Label + "Closed.");
                    }
                    else
                    {
                        AddOutput(pi.Label + e.ToString());
                    }

                    CleanupProtocol(pi);
                }
                finally
                {
                    pi.IsSendingPeriodicaly = false;
                    StopSendPeriodicaly.RaiseCanExecuteChanged();
                    RaisePropertyChanged(nameof(SelectedProtocolIsSendingPeriodicaly));

                    lock (pi)
                    {
                        pi.IsSending = false;
                    }
                }
            }
            else
            {
                try
                {
                    SendMessage(pi, random);
                }
                catch (IOException e)
                {
                    if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Closing)
                    {
                        AddOutput(pi.Label + "Closed.");
                    }
                    else
                    {
                        AddOutput(pi.Label + e.ToString());
                    }

                    CleanupProtocol(pi);
                }
                catch (Exception e)
                {
                    if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Closing)
                    {
                        AddOutput(pi.Label + "Closed.");
                    }
                    else
                    {
                        AddOutput(pi.Label + e.ToString());
                    }

                    CleanupProtocol(pi);
                }
                finally
                {
                    lock (pi)
                    {
                        pi.IsSending = false;
                    }
                }

            }
        }

        private void SendMessage(ProtocolInfo pi, Random randomGen)
        {
            var contentType = MessageOptions.SelectedContentType;
            byte[] data = CreateMessage(contentType, MessageOptions.GetMinLength(), MessageOptions.GetMaxLength(), MessageOptions.Random, MessageOptions.Content, randomGen);
            MessageOptions.Content = "";

            int messagesCount = (int)Math.Ceiling((double)data.Length / (double)Message.MaxLength);
            List<Message> messages = new List<Message>();
            int offset = 0;
            for (int i = 0; i < messagesCount; i++)
            {
                int msgLength;
                if (data.Length - offset > Message.MaxLength)
                {
                    msgLength = Message.MaxLength;
                }
                else
                {
                    msgLength = data.Length - offset;
                }

                byte[] msgData = new byte[msgLength];
                Array.Copy(data, offset, msgData, 0, msgLength);

                Message message = new Message(contentType, msgData);
                messages.Add(message);
            }

            lock (pi)
            {
                for (int i = 0; i < messages.Count; i++)
                {
                    var toSend = messages[i].Serialize();

                    pi.Protocol.Write(toSend, 0, toSend.Length);
                }
            }
        }
        private byte[] CreateMessage(MessageOptions.ContentType contentType, int minLength, int maxLength, bool random, string content, Random randomGen)
        {
            byte[] data = null;

            if (random)
            {
                if (minLength <= 0 || maxLength <= 0)
                {
                    throw new ArgumentException("Lenght is negative.");
                }
                if (minLength > maxLength)
                {
                    throw new ArgumentException("Min lenght is larger than max lenght.");
                }

                if (contentType == MessageOptions.ContentType.Hex)
                {
                    data = CreateRandomHex(minLength, maxLength, randomGen);
                }
                else if (contentType == MessageOptions.ContentType.String)
                {
                    data = CreateRandomString(minLength, maxLength, randomGen);
                }
            }
            else
            {
                if (contentType == MessageOptions.ContentType.String)
                {
                    data = Encoding.ASCII.GetBytes(content);
                }
                else if (contentType == MessageOptions.ContentType.Hex)
                {
                    data = ParseHex(content);
                }
            }

            return data;
        }
        private byte[] CreateRandomString(int minLength, int maxLength, Random randomGen)
        {
            var allowedChars = Encoding.ASCII.GetBytes("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");


            int length = randomGen.Next(minLength, maxLength + 1);
            byte[] ret = new byte[length];

            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = allowedChars[randomGen.Next(0, allowedChars.Length)];
            }

            return ret;
        }
        private byte[] CreateRandomHex(int minLength, int maxLength, Random randomGen)
        {
            int length = randomGen.Next(minLength, maxLength + 1);

            byte[] ret = new byte[length];

            randomGen.NextBytes(ret);

            return ret;
        }
        private byte[] ParseHex(string data)
        {
            var dataNoWS = new string(data.Where(x => char.IsWhiteSpace(x) == false).ToArray());
            if (dataNoWS.Length % 2 != 0)
            {
                throw new ArgumentException("Data isnt byte aligned.");
            }
            if (Regex.IsMatch(dataNoWS, "[^1234567890ABCDEFabcdef]"))
            {
                throw new ArgumentException("Data contains invalid character. Allowed are only hexadecimal numbers and whitespace.");
            }

            byte[] ret = new byte[dataNoWS.Length / 2];
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] = byte.Parse(dataNoWS.Substring(2 * i, 2), System.Globalization.NumberStyles.HexNumber);
            }

            return ret;
        }

        private async Task ReceiveTask(ProtocolInfo pi)
        {
            while (pi.CancelReceiveTask.IsCancellationRequested == false)
            {
                if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Open)
                {
                    try
                    {
                        Receive(pi);
                    }
                    catch (IOException e)
                    {
                        if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Closing)
                        {
                            AddOutput(pi.Label + "Closed.");
                        }
                        else
                        {
                            AddOutput(pi.Label + e.ToString());
                        }

                        CleanupProtocol(pi);
                        return;
                    }
                    catch (Exception e)
                    {
                        if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Closing)
                        {
                            AddOutput(pi.Label + "Closed.");
                        }
                        else
                        {
                            AddOutput(pi.Label + e.ToString());
                        }

                        CleanupProtocol(pi);
                        return;
                    }
                }
                else if (pi.Protocol != null && pi.Protocol.Status == LineProtocol.LineStatus.Closed || pi.Protocol.Status == LineProtocol.LineStatus.Closing)
                {
                    AddOutput(pi.Label + "Closed.");
                    CleanupProtocol(pi);
                    return;
                }

                await Task.Delay(1000);
            }
        }
        private void Receive(ProtocolInfo pi)
        {
            byte[] readBuffer = new byte[256];

            lock (pi)
            {
                for (int i = 0; i < 5; i++)
                {
                    int bRead = pi.Protocol.Read(readBuffer, 0, 256);

                    pi.Buffer.AddRange(readBuffer.Take(bRead));
                }
            }

            Message msg = new Message();
            while (msg.CanDeserialize(pi.Buffer))
            {
                msg.Deserialize(pi.Buffer);
                if (msg.Type == MessageOptions.ContentType.Hex)
                {
                    AddOutput(pi.Label + string.Concat(msg.Data.Select(x => x.ToString("X"))));
                }
                else if (msg.Type == MessageOptions.ContentType.String)
                {
                    AddOutput(pi.Label + "Received:" + Encoding.ASCII.GetString(msg.Data));
                }
            }
        }
        private void AssociateDevicesExecute()
        {
            Messenger.Default.Send(new NotificationMessageAction<List<DeviceInfo>[]>(MessageLabels.AssociateDevices,
                (List<DeviceInfo>[] dis) =>
                {
                    protocol1.Devices.Clear();
                    protocol2.Devices.Clear();

                    protocol1.Devices.AddRange(dis[0]);
                    protocol2.Devices.AddRange(dis[1]);
                    // TODO: remove protocols and stop
                }));
        }

        private void OpenManualExecute()
        {
            Messenger.Default.Send(new NotificationMessage(MessageLabels.OpenManual));
        }

        private void CleanupProtocol(ProtocolInfo pi)
        {
            lock (pi)
            {
                if (pi.Protocol == null)
                {
                    return;
                }

                StopSendPeriodicalyExecute(pi);

                if (pi.ReceiveTask != null)
                {
                    pi.CancelReceiveTask.Cancel();
                    pi.ReceiveTask = null;
                }

                if (pi.Protocol != null)
                {
                    pi.Protocol.Dispose();
                    pi.Protocol = null;
                }
            }
        }

        private void AddOutput(string mesasge)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(() =>
            {
                Output.Add(DateTime.Now.ToString("HH:mm:ss.fff") + " - " + mesasge);
            });
        }

        private void ProcessMessages(NotificationMessage m)
        {
            if(m.Notification == MessageLabels.Exiting)
            {
                CleanupProtocol(protocol1);
                CleanupProtocol(protocol2);
            }
        }
    }
}