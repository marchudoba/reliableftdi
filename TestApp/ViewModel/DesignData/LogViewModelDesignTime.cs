﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FtdiWrapper.Logging;
using static FtdiWrapper.Logging.Logger;

namespace TestApp.ViewModel.DesignData
{
    class LogViewModelDesignTime
    {
        public class FilterItem<T>
        {
            public T Value { get; set; }
            public bool Enabled { get; set; } = false;
        }

        public LogViewModelDesignTime()
        {
            var entry1 = new ExceptionLogEntry(Logger.Id.DeviceFactory, Logger.Level.Critical, new ArgumentNullException("param"));
            var entry2 = new ObjectLogEntry(Logger.Id.DeviceStream, Logger.Level.Information, new object());
            var entry3 = new TextLogEntry(Logger.Id.Protocol, Logger.Level.Verbose, "message fasf dsaf asdf adsf adsfa sdfas dfa sdf");

            Log.Add(entry1);
            Log.Add(entry2);
            Log.Add(entry3);
            Log.Add(entry3);
            Log.Add(entry3);
            Log.Add(entry3);
            Log.Add(entry3);
            Log.Add(entry3);
            Log.Add(entry3);

            foreach (var id in Enum.GetValues(typeof(Id)))
            {
                Ids.Add(new FilterItem<Id> { Value = (Id)id });
            }

            foreach (var level in Enum.GetValues(typeof(Level)))
            {
                Levels.Add(new FilterItem<Level> { Value = (Level)level });
            }
        }
        public ObservableCollection<FilterItem<Id>> Ids { get; private set; } = new ObservableCollection<FilterItem<Id>>();
        public ObservableCollection<FilterItem<Level>> Levels { get; private set; } = new ObservableCollection<FilterItem<Level>>();
        public ObservableCollection<LogEntry> Log { get; private set; } = new ObservableCollection<LogEntry>();
    }
}
