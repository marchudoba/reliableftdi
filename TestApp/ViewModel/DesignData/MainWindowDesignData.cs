﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.ViewModel.DesignData
{
    class MainWindowDesignData
    {
        public class MessageOptionsT : MessageOptions
        {
            public new bool HasErrors
            {
                get
                {
                    return true;
                }
            }
        }

        public MessageOptionsT MessageOptions { get; private set; } = new MessageOptionsT();

        public MainWindowDesignData()
        {
            Output.Add("test");
            Output.Add("test");
            Output.Add("test");
            Output.Add("test");


            MessageOptions.Errors.Add(new Tuple<string, ObservableCollection<string>>("errors1", new ObservableCollection<string>()));
            MessageOptions.Errors.Add(new Tuple<string, ObservableCollection<string>>("errors2", new ObservableCollection<string>()));

            MessageOptions.Errors[0].Item2.Add("error11");
            MessageOptions.Errors[0].Item2.Add("error12 dsfasdf asdf asdf adsfsadf asdf asdf asf sadf ads fsadf sdf sad f sad f asdf fsa fs d fs fas asd f asd");
            MessageOptions.Errors[1].Item2.Add("error21");
            MessageOptions.Errors[1].Item2.Add("error22 dsfasdf asdf asdf adsfsadf asdf asdf asf sadf ads fsadf sdf sad f sad f asdf fsa fs d fs fas asd f asd");
        }

        public Protocols SelectedProtocol { get; set; } = Protocols.Protocol1;
        public ObservableCollection<string> Output { get; private set; } = new ObservableCollection<string>();
    }
}
