﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FtdiWrapper;

namespace TestApp.ViewModel.DesignData
{
    class AssociateDevicesDesignData
    {
        public AssociateDevicesDesignData()
        {
            AddDevices();

            ProtocolDevices.Add(Devices[0]);
            ProtocolDevices.Add(Devices[1]);
        }

        public bool HasErrors { get { return false; } }
        public Protocols SelectedProtocol { get; set; } = Protocols.Protocol1;
        public ObservableCollection<object> Devices { get; private set; } = new ObservableCollection<object>();
        public ObservableCollection<object> ProtocolDevices { get; private set; } = new ObservableCollection<object>();

        private void AddDevices()
        {
            Devices.Add(new
            {
                IsOpen = false,
                IsHighSpeed = false,
                VendorId = 0xFFAA,
                HardwareId = 0xBBCC,
                LocationId = 1,
                SerialNumber = "serial#",
                Description = "description",
                DeviceType = DeviceType.Device232R
            });
            Devices.Add(new
            {
                IsOpen = false,
                IsHighSpeed = false,
                VendorId = 0xFFAA,
                HardwareId = 0xBBCC,
                LocationId = 2,
                SerialNumber = "serial#",
                Description = "description",
                DeviceType = DeviceType.Device232R
            });
            Devices.Add(new
            {
                IsOpen = false,
                IsHighSpeed = false,
                VendorId = 0xFFAA,
                HardwareId = 0xBBCC,
                LocationId = 3,
                SerialNumber = "serial#",
                Description = "description",
                DeviceType = DeviceType.Device232R
            });
            Devices.Add(new
            {
                IsOpen = false,
                IsHighSpeed = false,
                VendorId = 0xFFAA,
                HardwareId = 0xBBCC,
                LocationId = 4,
                SerialNumber = "serial#",
                Description = "description",
                DeviceType = DeviceType.Device232R
            });
        }
    }
}
