﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp.ViewModel
{
    class MessageLabels
    {
        public static string AssociateDevices { get { return "AssociateDevices"; } }
        public static string AssociateDevicesOk { get { return "AssociateDevicesOk"; } }
        public static string AssociateDevicesCancel { get { return "AssociateDevicesCancel"; } }
        public static string OpenLog { get { return "OpenLog"; } }
        public static string OpenFileNameDialog { get { return "OpenFileNameDialog"; } }
        public static string OpenManual { get { return "OpenManual"; } }
        public static string Exiting { get { return "Exiting"; } }
    }
}
