﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FtdiWrapper;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;

namespace TestApp.ViewModel
{
    // TODO: changes persist in view when cancel is pressed
    public class AssociateDevicesViewModel : ViewModelValidatorBase
    {
        public AssociateDevicesViewModel()
        {
            Refresh = new RelayCommand(RefreshExecute);
            AddDevices = new RelayCommand<IList>(AddDevicesExecute);
            RemoveDevices = new RelayCommand<IList>(RemoveDevicesExecute);
            Ok = new RelayCommand(OkExecute);
            Cancel = new RelayCommand(CancelExecute);
            
            AddRule(new DelegateRule(() => (Protocol1Devices.Count > 0 && Protocol2Devices.Count > 0 && Protocol1Devices.Count != Protocol2Devices.Count)==false, "Both protocols have some devices assigned but dont have same amount of devices. Either assign same amount of devices to both protocols or leave one of them empty. ", nameof(ProtocolDevices)));

            RefreshExecute();
        }

        private Protocols selectedProtocol = Protocols.Protocol1;

        public Protocols SelectedProtocol
        {
            get
            {
                return selectedProtocol;
            }
            set
            {
                Set(ref selectedProtocol, value);
                RefreshProtocolDevices();
            }
        }
        public ICommand Refresh { get; private set; }
        public ICommand AddDevices { get; private set; }
        public ICommand RemoveDevices { get; private set; }
        public ICommand Ok { get; private set; }
        public ICommand Cancel { get; private set; }


        public ObservableCollection<DeviceInfo> Devices { get; private set; } = new ObservableCollection<DeviceInfo>();
        public ObservableCollection<DeviceInfo> ProtocolDevices { get; private set; } = new ObservableCollection<DeviceInfo>();




        public List<DeviceInfo> Protocol1Devices { get; private set; } = new List<DeviceInfo>();
        public List<DeviceInfo> Protocol2Devices { get; private set; } = new List<DeviceInfo>();


        public void OkExecute()
        {
            if (Validate(nameof(ProtocolDevices)))
            {
                Messenger.Default.Send(new NotificationMessage(MessageLabels.AssociateDevicesOk));
            }
        }
        public void CancelExecute()
        {
            Messenger.Default.Send(new NotificationMessage(MessageLabels.AssociateDevicesCancel));
        }
        private void RefreshProtocolDevices()
        {
            ProtocolDevices.Clear();

            if (SelectedProtocol == Protocols.Protocol1)
            {
                foreach (var di in Protocol1Devices)
                {
                    ProtocolDevices.Add(di);
                }
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                foreach (var di in Protocol2Devices)
                {
                    ProtocolDevices.Add(di);
                }
            }
        }
        private void RefreshExecute()
        {
            Devices.Clear();
            ProtocolDevices.Clear();
            Protocol1Devices.Clear();
            Protocol2Devices.Clear();

            DeviceFactory.Default.RefreshDeviceInfo();

            foreach (var di in DeviceFactory.Default.DeviceInfo)
            {
                Devices.Add(di);
            }
        }
        private void AddDevicesExecute(IList selectedItems)
        {
            if (SelectedProtocol == Protocols.Protocol1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    var di = selectedItems[i];
                    Devices.Remove(di as DeviceInfo);
                    ProtocolDevices.Add(di as DeviceInfo);
                    Protocol1Devices.Add(di as DeviceInfo);
                }
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    var di = selectedItems[i];
                    Devices.Remove(di as DeviceInfo);
                    ProtocolDevices.Add(di as DeviceInfo);
                    Protocol2Devices.Add(di as DeviceInfo);
                }
            }
        }
        private void RemoveDevicesExecute(IList selectedItems)
        {
            if (SelectedProtocol == Protocols.Protocol1)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    var di = selectedItems[i];
                    Devices.Add(di as DeviceInfo);
                    ProtocolDevices.Remove(di as DeviceInfo);
                    Protocol1Devices.Remove(di as DeviceInfo);
                }
            }
            else if (SelectedProtocol == Protocols.Protocol2)
            {
                for (int i = selectedItems.Count - 1; i >= 0; i--)
                {
                    var di = selectedItems[i];
                    Devices.Add(di as DeviceInfo);
                    ProtocolDevices.Remove(di as DeviceInfo);
                    Protocol2Devices.Remove(di as DeviceInfo);
                }
            }
        }
    }
}
