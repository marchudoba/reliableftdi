﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using FtdiWrapper.Logging;
using GalaSoft.MvvmLight.Threading;
using GalaSoft.MvvmLight.Command;

namespace TestApp.ViewModel
{
    public class LogViewModel
    {
        public class FilterItem<T>
        {
            private bool enabled = true;

            public T Value { get; set; }
            public bool Enabled
            {
                get
                {
                    return enabled;
                }
                set
                {
                    enabled = value;
                    if (enabled == false)
                    {
                        if (Value.GetType() == typeof(Logger.Id))
                        {
                            Logger.Instance.ExclusionFilter.Add(((Logger.Id)(object)Value));
                        }
                        else
                        {
                            Logger.Instance.ExclusionFilter.Add(((Logger.Level)(object)Value));
                        }
                    }
                    else
                    {
                        if (Value.GetType() == typeof(Logger.Id))
                        {
                            Logger.Instance.ExclusionFilter.Remove(((Logger.Id)(object)Value));
                        }
                        else
                        {
                            Logger.Instance.ExclusionFilter.Remove(((Logger.Level)(object)Value));
                        }
                    }
                }
            }
        }

        public LogViewModel()
        {
            BindingOperations.EnableCollectionSynchronization(Log, new object());

            Logger.Instance.NewEntry += (sender, e) =>
              {
                  //DispatcherHelper.CheckBeginInvokeOnUI(() =>
                  //{
                      Log.Insert(0, e.NewEntry);

                      if (Log.Count > Logger.Instance.MaxLogSize)
                      {
                          Log.RemoveAt(Log.Count - 1);
                      }
                  //});
              };

            foreach (var id in Enum.GetValues(typeof(Logger.Id)))
            {
                Ids.Add(new FilterItem<Logger.Id> { Value = (Logger.Id)id });
            }

            foreach (var level in Enum.GetValues(typeof(Logger.Level)))
            {
                Levels.Add(new FilterItem<Logger.Level> { Value = (Logger.Level)level });
            }

            Ids.Where(x => x.Value == Logger.Id.NativeCall).First().Enabled = false;
            Ids.Where(x => x.Value == Logger.Id.DeviceStream).First().Enabled = false;
            Ids.Where(x => x.Value == Logger.Id.DeviceFactory).First().Enabled = false;
            Levels.Where(x => x.Value == Logger.Level.Verbose).First().Enabled = false;
        }
        
        public ObservableCollection<FilterItem<Logger.Id>> Ids { get; private set; } = new ObservableCollection<FilterItem<Logger.Id>>();
        public ObservableCollection<FilterItem<Logger.Level>> Levels { get; private set; } = new ObservableCollection<FilterItem<Logger.Level>>();
        public ObservableCollection<LogEntry> Log { get; private set; } = new ObservableCollection<LogEntry>();
    }
}
