﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace TestApp.ViewModel
{
    public class ViewModelValidatorBase : ViewModelBase, INotifyDataErrorInfo
    {

        private event EventHandler<DataErrorsChangedEventArgs> errorsChanged;
        public ObservableCollection<Tuple<string, ObservableCollection<string>>> Errors { get; private set; } = new ObservableCollection<Tuple<string, ObservableCollection<string>>>();
        private ObservableCollection<Tuple<string, List<IRule>>> rules = new ObservableCollection<Tuple<string, List<IRule>>>();
        private bool hasErrors = false;
        private int errorCount = 0;


        private ObservableCollection<string> GetErrorEntry(string name)
        {
            for (int i = 0; i < Errors.Count; i++)
            {
                if (Errors[i].Item1 == name)
                {
                    return Errors[i].Item2;
                }
            }

            return null;
        }
        private List<IRule> GetRuleEntry(string name)
        {
            for (int i = 0; i < rules.Count; i++)
            {
                if (rules[i].Item1 == name)
                {
                    return rules[i].Item2;
                }
            }

            return null;
        }

        /// <summary>
        /// Validates the property using all rules with <paramref name="name"/>.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns><c>true</c> if property contains valid value <c>false</c> otherwise.</returns>
        protected bool Validate([CallerMemberName]string name = "")
        {
            if (GetRuleEntry(name) == null)
            {
                rules.Add(new Tuple<string, List<IRule>>(name, new List<IRule>()));
            }

            bool ret = true;
            foreach (var rule in GetRuleEntry(name))
            {
                if (rule.Validate() == false)
                {
                    if (GetErrorEntry(name) == null)
                    {
                        Errors.Add(new Tuple<string, ObservableCollection<string>>(name, new ObservableCollection<string>()));
                    }

                    ret = false;
                    if (GetErrorEntry(name).Contains(rule.Error) == false)
                    {
                        GetErrorEntry(name).Add(rule.Error);

                        if (errorCount == 0)
                        {
                            HasErrors = true;
                        }

                        errorCount++;
                        errorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(name));
                    }
                }
                else
                {
                    RemoveError(name, rule.Error);
                }
            }
            return ret;
        }
        private void RemoveError(string propertyName, string error)
        {
            var errors = GetErrorEntry(propertyName);

            if (errors != null && errors.Contains(error) == true)
            {
                errors.Remove(error);

                if (errors.Count == 0)
                {
                    Errors.Remove(Errors.Where(x => x.Item1 == propertyName).First());
                }


                errorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));

                errorCount--;
                if (errorCount == 0)
                {
                    HasErrors = false;
                }
            }
        }
        protected void AddRule(IRule rule)
        {
            foreach (var propertyName in rule.PropertyNames)
            {
                if (GetRuleEntry(propertyName) == null)
                {
                    rules.Add(new Tuple<string, List<IRule>>(propertyName, new List<IRule>()));
                }

                GetRuleEntry(propertyName).Add(rule);
            }
        }

        protected void RemoveRule(IRule rule)
        {
            foreach (var propertyName in rule.PropertyNames)
            {
                var rules = GetRuleEntry(propertyName);
                rules.Remove(rule);
            }
        }

        public bool HasErrors
        {
            get
            {
                return hasErrors;
            }
            private set
            {
                Set(ref hasErrors, value);
            }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged
        {
            add
            {
                errorsChanged += value;
            }
            remove
            {
                errorsChanged -= value;
            }
        }

        public IEnumerable GetErrors(string propertyName)
        {
            return GetErrorEntry(propertyName);
        }
    }

    public interface IRule
    {
        string Error { get; }
        string[] PropertyNames { get; }
        bool Validate();
    }

    public class DelegateRule : IRule
    {
        private string error;
        private string[] propertyNames;
        private Func<bool> validate;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateRule"/> class.
        /// </summary>
        /// <param name="validate">The validate - false if validation fails.</param>
        /// <param name="error">The error.</param>
        /// <param name="propertyNames">The property names.</param>
        public DelegateRule(Func<bool> validate, string error, params string[] propertyNames)
        {
            this.error = error;
            this.propertyNames = propertyNames;
            this.validate = validate;
        }

        public string Error
        {
            get
            {
                return error;
            }
        }

        public string[] PropertyNames
        {
            get
            {
                return propertyNames;
            }
        }

        public bool Validate()
        {
            return validate();
        }
    }
}
