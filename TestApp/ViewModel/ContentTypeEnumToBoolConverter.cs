﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace TestApp.ViewModel
{
    class ContentTypeEnumToBoolConverter : IValueConverter
    {
        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value produced by the binding source.</param>
        /// <param name="targetType">The type of the binding target property.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null)
            {
                return null;
            }

            if (value is MessageOptions.ContentType && targetType == typeof(bool?))
            {
                MessageOptions.ContentType cType = (MessageOptions.ContentType)value;
                MessageOptions.ContentType param;

                if (Enum.TryParse(parameter.ToString(), out param))
                {
                    return cType == param;
                }
            }

            return null;
        }

        /// <summary>
        /// Converts a value.
        /// </summary>
        /// <param name="value">The value that is produced by the binding target.</param>
        /// <param name="targetType">The type to convert to.</param>
        /// <param name="parameter">The converter parameter to use.</param>
        /// <param name="culture">The culture to use in the converter.</param>
        /// <returns>
        /// A converted value. If the method returns null, the valid null value is used.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value == null || parameter == null)
            {
                return null;
            }

            if(value is bool? && targetType == typeof(MessageOptions.ContentType))
            {
                bool? val = (bool?)value;
                MessageOptions.ContentType param;

                if (Enum.TryParse(parameter.ToString(), out param))
                {
                    if(val.HasValue && val.Value)
                    {
                        return param;
                    }
                    else if(val.HasValue && val.Value == false)
                    {
                        if(param == MessageOptions.ContentType.Hex)
                        {
                            return MessageOptions.ContentType.String;
                        }
                        else
                        {
                            return MessageOptions.ContentType.Hex;
                        }
                    }
                }
            }

            return null;
        }
    }
}
