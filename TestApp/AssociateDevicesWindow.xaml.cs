﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using TestApp.ViewModel;

namespace TestApp
{
    /// <summary>
    /// Interaction logic for AssociateDevicesWindow.xaml
    /// </summary>
    public partial class AssociateDevicesWindow : Window
    {
        public AssociateDevicesWindow()
        {
            InitializeComponent();
            Messenger.Default.Register<NotificationMessage>(this, ProcessMessages);
        }

        private void ProcessMessages(NotificationMessage m)
        {
            if(m.Notification == MessageLabels.AssociateDevicesOk)
            {
                Messenger.Default.Unregister<NotificationMessage>(this, ProcessMessages);
                DialogResult = true;
            }
            else if(m.Notification == MessageLabels.AssociateDevicesCancel)
            {
                Messenger.Default.Unregister<NotificationMessage>(this, ProcessMessages);
                DialogResult = false;
            }
        }
    }
}
