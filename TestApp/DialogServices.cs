﻿using GalaSoft.MvvmLight.Messaging;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp.ViewModel;

namespace TestApp
{
    class DialogServices
    {
        public static void  Initialize()
        {
            Default = new DialogServices();
        }

        public DialogServices()
        {
            Messenger.Default.Register<NotificationMessageAction<string>>(this, ProcessMessages);
        }

        public static DialogServices Default { get; set; }

        private void ProcessMessages(NotificationMessageAction<string> m)
        {
            if (m.Notification == MessageLabels.OpenFileNameDialog)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.DefaultExt = "txt";
                saveFileDialog.Filter = "Text files(*.txt) | *.txt";
                saveFileDialog.AddExtension = true;
                if (saveFileDialog.ShowDialog() == true)
                {
                    m.Execute(saveFileDialog.FileName);
                }
            }
        }
    }
}
