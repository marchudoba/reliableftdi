﻿namespace FtdiWrapper.Exceptions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This exception is thrown if method from <see cref="Native.NativeMethods"/> doesn't return <see cref="Native.FT_STATUS.FT_OK"/>
    /// </summary>
    /// <seealso cref="Exception" />
    /// <autogeneratedoc />
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1064:ExceptionsShouldBePublic", Justification = "This exception is thrown from NativeMethods and should always be handled by the library")]
    [Serializable]
    internal class FTDIException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FTDIException"/> class.
        /// </summary>
        /// <autogeneratedoc />
        public FTDIException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FTDIException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        /// <autogeneratedoc />
        public FTDIException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FTDIException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="inner">The inner.</param>
        /// <autogeneratedoc />
        public FTDIException(string message, Exception inner) : base(message, inner)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FTDIException"/> class.
        /// </summary>
        /// <param name="status">The status.</param>
        /// <autogeneratedoc />
        internal FTDIException(Native.FT_STATUS status) : this("Method returned " + status + ".")
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FTDIException"/> class.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        /// <autogeneratedoc />
        protected FTDIException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }
    }
}
