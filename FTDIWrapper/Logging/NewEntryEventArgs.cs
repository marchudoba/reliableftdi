﻿namespace FtdiWrapper.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides data for <see cref="Logger.NewEntry"/> event.
    /// </summary>
    /// <seealso cref="System.EventArgs" />
    public class NewEntryEventArgs : EventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NewEntryEventArgs"/> class.
        /// </summary>
        /// <param name="entry">The entry.</param>
        public NewEntryEventArgs(LogEntry entry)
        {
            NewEntry = entry;
        }

        /// <summary>
        /// Gets the new entry.
        /// </summary>
        /// <value>
        /// The new entry.
        /// </value>
        public LogEntry NewEntry { get; private set; }
    }
}
