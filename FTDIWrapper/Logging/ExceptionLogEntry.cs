﻿namespace FtdiWrapper.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Log entry for storing exceptions.
    /// </summary>
    /// <seealso cref="FtdiWrapper.Logging.LogEntry" />
    public class ExceptionLogEntry : LogEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExceptionLogEntry"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="level">The type.</param>
        /// <param name="exception">The exception.</param>
        public ExceptionLogEntry(Logger.Id id, Logger.Level level, Exception exception) : base(id, level)
        {
            Exception = exception;
        }

        /// <summary>
        /// Gets the exception.
        /// </summary>
        /// <value>
        /// The exception.
        /// </value>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0}\t{1}\t{2}\t{3}", Timestamp.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.CurrentCulture), Id, Level, Exception.ToString());
        }
    }
}
