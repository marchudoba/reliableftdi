﻿namespace FtdiWrapper.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Log entry for storing objects.
    /// </summary>
    /// <seealso cref="FtdiWrapper.Logging.LogEntry" />
    public class ObjectLogEntry : LogEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObjectLogEntry"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="arg">The argument.</param>
        public ObjectLogEntry(Logger.Id id, Logger.Level type, object arg) : base(id, type)
        {
            Object = arg;
        }

        /// <summary>
        /// Gets the parameter.
        /// </summary>
        /// <value>
        /// The parameter.
        /// </value>
        public object Object { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0}\t{1}\t{2}\t{3}", Timestamp.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.CurrentCulture), Id, Level, this.Object.ToString());
        }
    }
}
