﻿namespace FtdiWrapper.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents base class for log entry.
    /// </summary>
    public class LogEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogEntry" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="level">The level.</param>
        public LogEntry(Logger.Id id, Logger.Level level)
        {
            Timestamp = DateTime.Now;
            Id = id;
            Level = level;
        }

        /// <summary>
        /// Gets the identifier of logged event.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Logger.Id Id { get; private set; }

        /// <summary>
        /// Gets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; private set; }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public Logger.Level Level { get; private set; }
    }
}
