﻿namespace FtdiWrapper.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    /// <summary>
    /// This class is used to serialize object for <see cref="Logger"/>.
    /// </summary>
    public sealed class Dumper
    {
        private Dumper()
        {
        }

        /// <summary>
        /// Dumps the <paramref name="arg" />.
        /// </summary>
        /// <param name="arg">The argument.</param>
        /// <remarks>
        /// Uses <see cref="CultureInfo.CurrentCulture"/> as format provider.
        /// </remarks>
        /// <returns>
        /// Serialized arg.
        /// </returns>
        public static string Dump(object arg)
        {
            string dump = string.Empty;
            if (arg != null)
            {
                Type type = arg.GetType();

                var properties = type.GetProperties();

                foreach (var property in properties)
                {
                    dump += string.Format(CultureInfo.CurrentCulture, "{0} - {1}, ", property.Name, property.GetValue(arg));
                }
            }

            return dump;
        }

        /// <summary>
        /// Dumps the <paramref name="arg" />.
        /// </summary>
        /// <param name="formatProvider">The format provider.</param>
        /// <param name="arg">The argument.</param>
        /// <returns>
        /// Serialized arg.
        /// </returns>
        public static string Dump(IFormatProvider formatProvider, object arg)
        {
            string dump = string.Empty;
            if (arg != null)
            {
                Type type = arg.GetType();

                var properties = type.GetProperties();

                foreach (var property in properties)
                {
                    dump += string.Format(formatProvider, "{0} - {1}, ", property.Name, property.GetValue(arg));
                }
            }

            return dump;
        }
    }
}
