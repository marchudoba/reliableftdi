﻿namespace FtdiWrapper.Logging
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Log entry for text messages.
    /// </summary>
    /// <seealso cref="FtdiWrapper.Logging.LogEntry" />
    public class TextLogEntry : LogEntry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextLogEntry"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="message">The message.</param>
        public TextLogEntry(Logger.Id id, Logger.Level type, string message) : base(id, type)
        {
            Message = message;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextLogEntry" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The arguments.</param>
        /// <remarks>
        /// Message will be a result of string.Format(message, args). Uses <see cref="CultureInfo.CurrentCulture"/> as format provider.
        /// </remarks>
        public TextLogEntry(Logger.Id id, Logger.Level type, string message, params object[] args) : this(id, type, CultureInfo.CurrentCulture, message, args)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextLogEntry" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="type">The type.</param>
        /// <param name="formatProvider">The format provider.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The arguments.</param>
        /// <remarks>
        /// Message will be a result of string.Format(message, args).
        /// </remarks>
        public TextLogEntry(Logger.Id id, Logger.Level type, IFormatProvider formatProvider, string message, params object[] args) : base(id, type)
        {
            Message = string.Format(formatProvider, message, args);
        }

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture, "{0}\t{1}\t{2}\t{3}", Timestamp.ToString("dd.MM.yyyy HH:mm:ss.fff", CultureInfo.CurrentCulture), Id, Level, Message);
        }
    }
}
