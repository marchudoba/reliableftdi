﻿namespace FtdiWrapper
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Exceptions;
    using Logging;
    using Native;
    using Properties;

    /// <summary>
    /// Provides <see cref="Stream" /> for device.
    /// </summary>
    /// <remarks>
    /// DeviceStream doesn't provide any reliability in terms of data transmission - use <see cref="Messaging.Communicator" />
    /// </remarks>
    /// <seealso cref="System.IO.Stream" />
    public partial class DeviceStream : Stream
    {
        private readonly object openLock = new object();
        private readonly DeviceInfo deviceInfo;

        private readonly int defaultBaudrate = Settings.Instance.DefaultBaudRate;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceStream" /> class.
        /// </summary>
        /// <param name="deviceInfo">The device information.</param>
        /// <exception cref="ArgumentNullException">Thrown when deviceInfo is null.</exception>
        public DeviceStream(DeviceInfo deviceInfo)
        {
            if (deviceInfo == null)
            {
                throw new ArgumentNullException("deviceInfo");
            }

            this.deviceDriver = deviceInfo.DeviceDriver;
            this.deviceInfo = deviceInfo;
            this.handle = deviceInfo.Handle;
        }

        /// <summary>
        /// Gets the device information.
        /// </summary>
        /// <value>
        /// The device information.
        /// </value>
        public DeviceInfo DeviceInfo
        {
            get
            {
                return deviceInfo;
            }
        }

        /// <summary>
        /// Opens the device with specified baud rate.
        /// </summary>
        /// <param name="baudRate">The baud rate.</param>
        /// <exception cref="System.IO.IOException">
        /// Device failed to open.
        /// or
        /// Failed to set data characteristics.
        /// or
        /// Failed to set baud rate.
        /// </exception>
        /// <exception cref="ObjectDisposedException">Thrown when operation is performed on a disposed object.</exception>
        public void Open(int baudRate)
        {
            ThrowIfDisposed();
            if (baudRate <= 0)
            {
                throw new ArgumentOutOfRangeException("baudRate");
            }

            lock (openLock)
            {
                if (deviceInfo.IsOpen == true)
                {
                    return;
                }

                try
                {
                    deviceInfo.Handle = deviceDriver.OpenByLocation((uint)deviceInfo.LocationId);
                    handle = deviceInfo.Handle;
                }
                catch (TimeoutException e)
                {
                    Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                    throw new IOException("Device failed to open.", e);
                }
                catch (FTDIException e)
                {
                    Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                    throw new IOException("Device failed to open.", e);
                }

                if (deviceInfo.Handle != IntPtr.Zero)
                {
                    deviceInfo.IsOpen = true;
                }
                else
                {
                    // this shouldn't be reached
                    throw new IOException("Device failed to open.");
                }

                try
                {
                    deviceDriver.SetDataCharacteristics(deviceInfo.Handle, Native.Ftd2XX.WordLength.Bits8, Native.Ftd2XX.StopBits.Bits1, Native.Ftd2XX.Parity.None);
                }
                catch (TimeoutException e)
                {
                    Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                    throw new IOException("Failed to set data characteristics.", e);
                }
                catch (FTDIException e)
                {
                    Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                    throw new IOException("Failed to set data characteristics.", e);
                }

                try
                {
                    deviceDriver.SetBaudRate(deviceInfo.Handle, (uint)baudRate);
                }
                catch (TimeoutException e)
                {
                    Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                    throw new IOException("Failed to set baud rate.", e);
                }
                catch (FTDIException e)
                {
                    Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                    throw new IOException("Failed to set baud rate.", e);
                }

                Flush();
            }
        }

        /// <summary>
        /// Opens the device with default values.
        /// </summary>
        /// <exception cref="System.IO.IOException">
        /// Device failed to open.
        /// or
        /// Failed to set data characteristics.
        /// or
        /// Failed to set baud rate.
        /// </exception>
        /// <remarks>
        /// <list type="table">
        ///     <listheader>
        ///         <term>Default values.</term>
        ///     </listheader>
        ///     <item>
        ///         <term>baud rate</term>
        ///         <description>1500000</description>
        ///     </item>
        /// </list>
        /// </remarks>
        public void Open()
        {
            Open(defaultBaudrate);
        }

        /// <summary>
        /// Registers for events.
        /// </summary>
        /// <param name="events">The events.</param>
        /// <returns>
        ///   <see cref="EventWaitHandle" /> with not signaled inital state and <see cref="EventResetMode.AutoReset" />
        /// </returns>
        /// <exception cref="IOException">Failed to register for receive event</exception>
        internal EventWaitHandle RegisterForEvents(Ftd2XX.Events events)
        {
            ThrowIfDisposed();

            EventWaitHandle eventHandle = null;
            EventWaitHandle ret = null;
            try
            {
                eventHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
                RegisterForEvents(events, eventHandle);

                ret = eventHandle;
                eventHandle = null;
            }
            catch (FTDIException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);
                
                throw new IOException("Failed to register for receive event", e);
            }
            finally
            {
                if (eventHandle != null)
                {
                    eventHandle.Dispose();
                }
            }

            return ret;
        }

        /// <summary>
        /// Registers for events with <paramref name="eventHandle"/>.
        /// </summary>
        /// <param name="events">The events.</param>
        /// <param name="eventHandle">The event handle.</param>
        /// <exception cref="IOException">Failed to register for receive event</exception>
        private void RegisterForEvents(Ftd2XX.Events events, EventWaitHandle eventHandle)
        {
            OpenIfNotOpen();
            try
            {
                deviceDriver.SetEventNotification(deviceInfo.Handle, events, eventHandle);
            }
            catch (TimeoutException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Failed to register for receive events.", e);
            }
            catch (FTDIException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Failed to register for receive events.", e);
            }
        }

        private void OpenIfNotOpen()
        {
            if (deviceInfo.IsOpen == false)
            {
                Open();
            }
        }
    }

    /// <summary>
    /// Provides <see cref="Stream" /> for device.
    /// </summary>
    public partial class DeviceStream : Stream
    {
        private readonly List<byte> readBuffer = new List<byte>();
        private readonly Native.Ftd2XX deviceDriver;
        private IntPtr handle;
        private bool isDisposed = false;

        ~DeviceStream()
        {
            Dispose(false);
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports reading. Will always return true.
        /// </summary>
        public override bool CanRead
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports writing. Will always return true.
        /// </summary>
        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets or sets the position within the current stream.
        /// </summary>
        /// <exception cref="System.NotSupportedException">Thrown when this is called.</exception>
        public override long Position
        {
            get
            {
                throw new NotSupportedException();
            }

            set
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Gets a value indicating whether the current stream supports seeking. Will always return false.
        /// </summary>
        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the length in bytes of the stream.
        /// </summary>
        /// <exception cref="System.NotSupportedException">Thrown when this is called.</exception>
        public override long Length
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        /// <summary>
        /// Sets the length of the current stream.
        /// </summary>
        /// <param name="value">The desired length of the current stream in bytes.</param>
        /// <exception cref="System.NotSupportedException">Thrown when this is called.</exception>
        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Sets the position within the current stream.
        /// </summary>
        /// <param name="offset">A byte offset relative to the <paramref name="origin" /> parameter.</param>
        /// <param name="origin">A value of type <see cref="T:System.IO.SeekOrigin" /> indicating the reference point used to obtain the new position.</param>
        /// <returns>
        /// The new position within the current stream.
        /// </returns>
        /// <exception cref="System.NotSupportedException">Thrown when this is called.</exception>
        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Reads a sequence of bytes from the current stream and advances the position within the stream by the number of bytes read.
        /// </summary>
        /// <param name="buffer">An array of bytes. When this method returns, the buffer contains the specified byte array with the values between <paramref name="offset" /> and (<paramref name="offset" /> + <paramref name="count" /> - 1) replaced by the bytes read from the current source.</param>
        /// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin storing the data read from the current stream.</param>
        /// <param name="count">The maximum number of bytes to be read from the current stream.</param>
        /// <returns>
        /// The total number of bytes read into the buffer. This can be less than the number of bytes requested if that many bytes are not currently available, or zero (0) if the end of the stream has been reached.
        /// </returns>
        /// <exception cref="System.ArgumentNullException">Thrown when buffer is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when offset is negative
        /// or
        /// count is zero or negative.</exception>
        /// <exception cref="System.ArgumentException">Sum of offset and count is greater than the buffer length.</exception>
        /// <exception cref="System.IO.IOException">Thrown when read from device fails.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when buffer is null.</exception>
        public override int Read(byte[] buffer, int offset, int count)
        {
            OpenIfNotOpen();
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            if (count <= 0)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            if (offset + count > buffer.Length)
            {
                throw new ArgumentException("Sum of offset and count is greater than the buffer length.");
            }

            try
            {
                uint bytesToRead = deviceDriver.GetQueueStatus(deviceInfo.Handle);

                byte[] buf = new byte[bytesToRead];
                uint bytesReallyRead = deviceDriver.Read(deviceInfo.Handle, buf, bytesToRead);
                Debug.Assert(bytesToRead == bytesReallyRead, "Read different number of bytes then reported by get GetQueueStatus.");

                readBuffer.AddRange(buf.Take((int)bytesReallyRead));
            }
            catch (TimeoutException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Read failed.", e);
            }
            catch (FTDIException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Read failed.", e);
            }

            int bytesRead = 0;
            int index = 0;
            while (bytesRead < count && index < readBuffer.Count)
            {
                buffer[offset + index] = readBuffer[index];
                index++;
                bytesRead++;
            }

            readBuffer.RemoveRange(0, bytesRead);

            Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Verbose, Resources.DeviceWithIdReadB, DeviceInfo.LocationId, bytesRead);

            return bytesRead;
        }

        /// <summary>
        /// When overridden in a derived class, writes a sequence of bytes to the current stream and advances the current position within this stream by the number of bytes written.
        /// </summary>
        /// <param name="buffer">An array of bytes. This method copies <paramref name="count" /> bytes from <paramref name="buffer" /> to the current stream.</param>
        /// <param name="offset">The zero-based byte offset in <paramref name="buffer" /> at which to begin copying bytes to the current stream.</param>
        /// <param name="count">The number of bytes to be written to the current stream.</param>
        /// <exception cref="System.ArgumentNullException">Thrown when <paramref name="buffer"/> is null.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// Thrown when offset is negative
        /// or
        /// count zero or negative.
        /// </exception>
        /// <exception cref="System.ArgumentException">Sum of offset and count is greater than length of the buffer.</exception>
        /// <exception cref="System.IO.IOException">Thrown when write to device fails.</exception>
        public override void Write(byte[] buffer, int offset, int count)
        {
            OpenIfNotOpen();
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            if (count <= 0)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            if (offset + count > buffer.Length)
            {
                throw new ArgumentException("Sum of offset and count is greater than the buffer length.");
            }

            byte[] buf = new byte[count];
            Array.Copy(buffer, offset, buf, 0, count);

            try
            {
                uint bytesWritten = deviceDriver.Write(deviceInfo.Handle, buf, (uint)count);
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Verbose, Resources.DeviceWithIdWrittenB, DeviceInfo.LocationId, bytesWritten);
                Debug.Assert(bytesWritten == (uint)count, "Written less bytes than expected.");
            }
            catch (FTDIException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Write failed.", e);
            }
            catch (TimeoutException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Write failed.", e);
            }
        }

        /// <summary>
        /// Clears all buffers for this stream and causes any buffered data to be written to the underlying device.
        /// </summary>
        /// <exception cref="ObjectDisposedException">Thrown when operation is performed on a disposed object.</exception>
        public override void Flush()
        {
            ThrowIfDisposed();
            try
            {
                deviceDriver.Purge(DeviceInfo.Handle, Ftd2XX.Buffers.ReceiveBuffer | Ftd2XX.Buffers.TransmitBuffer);
            }
            catch (FTDIException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Failed to purge stream.", e);
            }
            catch (TimeoutException e)
            {
                Logger.Instance.Log(Logger.Id.DeviceStream, Logger.Level.Error, Resources.DeviceWithId + e.ToString(), DeviceInfo.LocationId);

                throw new IOException("Failed to purge stream.", e);
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.IO.Stream" /> and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            isDisposed = true;
            base.Dispose(disposing);

            if (handle != IntPtr.Zero)
            {
                try
                {
                    deviceDriver.Close(handle);
                    DeviceInfo.IsOpen = false;
                }
                catch (FTDIException)
                {
                }
                catch (TimeoutException)
                {
                }

                handle = IntPtr.Zero;
            }
        }

        private void ThrowIfDisposed()
        {
            if (isDisposed)
            {
                throw new ObjectDisposedException("DeviceStream");
            }
        }
    }
}
