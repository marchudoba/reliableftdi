﻿namespace FtdiWrapper.Protocols
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This class allows serialization and deserialization of <see cref="ProtocolFrame"/>.
    /// </summary>
    /// <remarks>
    /// Control frame format: 1 type, 2 seq#, 2 crc.
    /// Data frame format: 1 type, 2 seq#, 2 content length, 2 crc, 2 data crc, content.
    /// </remarks>
    internal class FrameSerializer
    {
        /// <summary>
        /// Gets the length of the control header.
        /// </summary>
        /// <value>
        /// The length of the control header.
        /// </value>
        public int ControlHeaderLength
        {
            get
            {
                return 5;
            }
        }

        /// <summary>
        /// Gets the length of the data header.
        /// </summary>
        /// <value>
        /// The length of the data header.
        /// </value>
        public int DataHeaderLength
        {
            get
            {
                return 9;
            }
        }

        /// <summary>
        /// Determines whether this instance can deserialize the specified buffer.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <returns>
        ///   <c>true</c> if this instance can deserialize the specified buffer; otherwise, <c>false</c>.
        /// </returns>
        public bool CanDeserialize(List<byte> buffer)
        {
            if (buffer.Count < 1)
            {
                return false;
            }

            ProtocolFrame.FrameType type = (ProtocolFrame.FrameType)buffer[0];
            if (type == ProtocolFrame.FrameType.Data)
            {
                if (buffer.Count < DataHeaderLength)
                {
                    return false;
                }

                int crc = Utils.Int32From2ByteArray(buffer, 5);

                if (Crc16.GetChecksum(buffer, 0, 5) != crc)
                {
                    // TODO: how much to remove
                    buffer.RemoveRange(0, 1);
                    return CanDeserialize(buffer);
                }

                int contentLenght = Utils.Int32From2ByteArray(buffer, 3);

                if (buffer.Count < DataHeaderLength + contentLenght)
                {
                    return false;
                }

                int dataCrc = Utils.Int32From2ByteArray(buffer, 7);

                if (Crc16.GetChecksum(buffer, DataHeaderLength, contentLenght) == dataCrc)
                {
                    return true;
                }
                else
                {
                    // TODO: how much to remove
                    buffer.RemoveRange(0, 1);
                    return CanDeserialize(buffer);
                }
            }
            else
            {
                if (buffer.Count < ControlHeaderLength)
                {
                    return false;
                }

                int crc = Utils.Int32From2ByteArray(buffer, 3);

                if (Crc16.GetChecksum(buffer, 0, 3) == crc)
                {
                    return true;
                }
                else
                {
                    // TODO: how much to remove
                    buffer.RemoveRange(0, 1);
                    return CanDeserialize(buffer);
                }
            }
        }

        /// <summary>
        /// Deserializes the specified buffer.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <returns>Deserialized frame.</returns>
        public ProtocolFrame Deserialize(List<byte> buffer)
        {
            ProtocolFrame.FrameType type = (ProtocolFrame.FrameType)buffer[0];
            if (type == ProtocolFrame.FrameType.Data)
            {
                int sequenceNumber = Utils.Int32From2ByteArray(buffer, 1);
                int contentLenght = Utils.Int32From2ByteArray(buffer, 3);

                // int crc = Utils.Int32From2ByteArray(buffer, 5);
                // int dataCrc = Utils.Int32From2ByteArray(buffer, 7);
                byte[] data = buffer.GetRange(DataHeaderLength, contentLenght).ToArray();

                buffer.RemoveRange(0, DataHeaderLength + contentLenght);

                ProtocolFrame ret = new ProtocolFrame(type, sequenceNumber, data);
                return ret;
            }
            else
            {
                int sequenceNumber = Utils.Int32From2ByteArray(buffer, 1);

                // int crc = Utils.Int32From2ByteArray(buffer, 3);
                buffer.RemoveRange(0, ControlHeaderLength);

                ProtocolFrame ret = new ProtocolFrame(type, sequenceNumber, null);
                return ret;
            }
        }

        /// <summary>
        /// Serializes the specified frame.
        /// </summary>
        /// <param name="frame">The frame.</param>
        /// <returns>Serialized frame.</returns>
        public byte[] Serialize(ProtocolFrame frame)
        {
            if (frame.Type == ProtocolFrame.FrameType.Data)
            {
                byte[] buffer = new byte[DataHeaderLength + frame.Content.Length];
                
                // 0
                buffer[0] = (byte)frame.Type;

                // 1 2
                Utils.Int32To2ByteArray(frame.SequenceNumber, buffer, 1);

                // 3 4
                Utils.Int32To2ByteArray(frame.Content.Length, buffer, 3);

                // 5 6
                Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 5), buffer, 5);

                // 7 8
                Utils.Int32To2ByteArray(Crc16.GetChecksum(frame.Content, 0, frame.Content.Length), buffer, 7);
                Array.Copy(frame.Content, 0, buffer, 9, frame.Content.Length);

                return buffer;
            }
            else
            {
                byte[] buffer = new byte[5];

                // 0
                buffer[0] = (byte)frame.Type;

                // 1 2
                Utils.Int32To2ByteArray(frame.SequenceNumber, buffer, 1);

                // 3 4
                Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 3), buffer, 3);
                return buffer;
            }
        }
    }
}
