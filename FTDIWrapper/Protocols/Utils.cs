﻿namespace FtdiWrapper.Protocols
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Utility methods.
    /// </summary>
    public sealed class Utils
    {
        private Utils()
        {
        }

        /// <summary>
        /// Converts byte array to int32.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <returns>
        /// Created int.
        /// </returns>
        /// <remarks>
        /// If byte array is {0xAA, 0xBB, 0xCC, 0xDD} then resulting int is 0xAABBCCDD.
        /// </remarks>
        /// <exception cref="ArgumentNullException">Thrown when buffer is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when offset isnt in [0, int.MaxValue - 3] range.</exception>
        /// <exception cref="ArgumentException">Thrown when buffer is smaller than offset + 4.</exception>
        public static int Int32FromByteArray(IEnumerable<byte> buffer, int offset)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (buffer.Count() < offset + 4)
            {
                throw new ArgumentException("Buffer is too small. Buffer has to contain 4B starting from offset.");
            }

            if (offset > int.MaxValue - 4)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            int val = 0;
            val |= (int)buffer.ElementAt(offset + 0) << 24;
            val |= (int)buffer.ElementAt(offset + 1) << 16;
            val |= (int)buffer.ElementAt(offset + 2) << 8;
            val |= (int)buffer.ElementAt(offset + 3) << 0;
            return val;
        }

        /// <summary>
        /// Coverts int32 to byte array.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <remarks>
        /// Inverse function to <see cref="Int32FromByteArray(IEnumerable{byte}, int)"/>.
        /// </remarks>
        /// <exception cref="ArgumentNullException">Thrown when buffer is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when offset isnt in [0,int.MaxValue-3] range.</exception>
        /// <exception cref="ArgumentException">Thrown when buffer is too small.</exception>
        public static void Int32ToByteArray(int value, byte[] buffer, int offset)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            if (buffer.Length < offset + 4)
            {
                throw new ArgumentException("Buffer is too small. Buffer has to be at least offset + 4 long.");
            }

            if (offset > int.MaxValue - 4)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            buffer[offset + 0] = (byte)((value & 0xFF000000) >> 24);
            buffer[offset + 1] = (byte)((value & 0x00FF0000) >> 16);
            buffer[offset + 2] = (byte)((value & 0x0000FF00) >> 8);
            buffer[offset + 3] = (byte)((value & 0x000000FF) >> 0);
        }

        /// <summary>
        /// Converts int32 to byte array of size 2.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <exception cref="ArgumentOutOfRangeException"><paramref name="buffer"/> is negative.
        /// or
        /// <paramref name="offset"/> is too large.</exception>
        /// <exception cref="ArgumentNullException"><paramref name="buffer"/> is null.</exception>
        /// <exception cref="ArgumentException">Value is greater than 65535.
        /// or
        /// Buffer is too small. Buffer has to be at least offset + 4 long.</exception>
        public static void Int32To2ByteArray(int value, byte[] buffer, int offset)
        {
            if (value > 0xFFFF)
            {
                throw new ArgumentOutOfRangeException("value");
            }

            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            if (buffer.Length < offset + 2)
            {
                throw new ArgumentException("Buffer is too small. Buffer has to be at least offset + 2 long.");
            }

            if (offset > int.MaxValue - 2)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            buffer[offset] = (byte)(value >> 8);
            buffer[offset + 1] = (byte)value;
        }

        /// <summary>
        /// Converts byte array of size 2 to int32.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <returns>Converted integer.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="buffer"/> is null.</exception>
        /// <exception cref="ArgumentException">Buffer is too small. Buffer has to contain 4B starting from offset.</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// <paramref name="offset"/> is negative.
        /// or
        /// <paramref name="offset"/> is too large.
        /// </exception>
        public static int Int32From2ByteArray(IEnumerable<byte> buffer, int offset)
        {
            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (buffer.Count() < offset + 2)
            {
                throw new ArgumentException("Buffer is too small. Buffer has to contain 2B starting from offset.");
            }

            if (offset > int.MaxValue - 2)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            int val = buffer.ElementAt(offset) << 8;
            val |= buffer.ElementAt(offset + 1);

            return val;
        }
    }
}
