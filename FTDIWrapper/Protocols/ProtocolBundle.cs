﻿namespace FtdiWrapper.Protocols
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using FtdiWrapper.Logging;
    using FtdiWrapper.Properties;

    /// <summary>
    /// This class provides layer of redundacy above <see cref="LineProtocol"/>.
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class ProtocolBundle : IDisposable
    {
        private readonly List<Line> protocols = new List<Line>();
        private readonly CancellationTokenSource recoveryTasksCTS = new CancellationTokenSource();
        private readonly int maxDataLength = 65535;
        private int nextSequenceNumberSend = 0;
        private int nextSequenceNumberReceive = 0;
        private List<byte> readBuffer = new List<byte>();
        private bool disposedValue = false;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolBundle"/> class.
        /// </summary>
        /// <param name="deviceInfo">The device information.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="deviceInfo"/> is null.</exception>
        /// <exception cref="System.ArgumentException">
        /// One of the provided device infos is null.
        /// or
        /// No device info provided.
        /// </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "All object are disposed.")]
        public ProtocolBundle(params DeviceInfo[] deviceInfo)
        {
            if (deviceInfo == null)
            {
                throw new ArgumentNullException("deviceInfo");
            }

            if (deviceInfo.Contains(null))
            {
                throw new ArgumentException("One of the provided device infos is null.");
            }

            if (deviceInfo.Length == 0)
            {
                throw new ArgumentException("No device info provided.");
            }

            LineProtocol protocol = null;
            try
            {
                foreach (var di in deviceInfo)
                {
                    protocol = new LineProtocol(di);
                    var line = new Line(protocol);
                    protocols.Add(line);

                    protocol = null;
                }
            }
            finally
            {
                if (protocol != null)
                {
                    protocol.Dispose();

                    foreach (var line in protocols)
                    {
                        if (line?.Protocol != protocol)
                        {
                            line.Protocol?.Dispose();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public LineProtocol.LineStatus Status
        {
            get
            {
                bool allOpen = true;
                bool allClosed = true;
                for (int i = 0; i < protocols.Count; i++)
                {
                    var protocol = protocols[i];
                    if (protocol.State == Line.Status.Ok)
                    {
                        switch (protocol.Protocol.Status)
                        {
                            case LineProtocol.LineStatus.Faulted:
                                return LineProtocol.LineStatus.Faulted;
                            case LineProtocol.LineStatus.Closing:
                                return LineProtocol.LineStatus.Closing;
                            case LineProtocol.LineStatus.Listening:
                                return LineProtocol.LineStatus.Listening;
                            case LineProtocol.LineStatus.Opening:
                                return LineProtocol.LineStatus.Opening;
                            case LineProtocol.LineStatus.Open:
                                allClosed = false;
                                break;
                            case LineProtocol.LineStatus.Closed:
                                allOpen = false;
                                break;
                        }

                        if (allOpen)
                        {
                            return LineProtocol.LineStatus.Open;
                        }
                        else if (allClosed)
                        {
                            return LineProtocol.LineStatus.Closed;
                        }
                    }
                }

                return LineProtocol.LineStatus.Faulted;
            }
        }

        /// <summary>
        /// Opens this instance.
        /// </summary>
        /// <param name="maxWaitTimeMillis">The maximum wait time millis.</param>
        /// <returns>
        /// true if all lines are open; otherwise false
        /// </returns>
        /// <exception cref="System.IO.IOException">Failed to open protocol.</exception>
        public bool Open(int maxWaitTimeMillis)
        {
            Task<bool>[] openTasks;
            lock (protocols)
            {
                openTasks = new Task<bool>[protocols.Count];
                for (int i = 0; i < protocols.Count; i++)
                {
                    if (protocols[i].State == Line.Status.Ok)
                    {
                        var protocol = protocols[i];
                        openTasks[i] = Task.Run(() => protocol.Protocol.Open(maxWaitTimeMillis));
                    }
                }
            }

            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Verbose, Resources.AttemptingToOpenProtocols, string.Concat(protocols.Select(x => string.Format(CultureInfo.CurrentCulture, "{0};", x.Protocol.DeviceInfo.LocationId))));

            for (int i = 0; i < openTasks.Length; i++)
            {
                try
                {
                    openTasks[i]?.Wait();
                }
                catch (AggregateException e)
                {
                    Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Error, Resources.ErrorOpeningProtocolException, protocols[i].Protocol.DeviceInfo.LocationId, e);

                    throw new IOException("Failed to open protocol " + protocols[i].Protocol.DeviceInfo.LocationId + ".", e);
                }
            }

            return openTasks.Where(x => x != null).Select(x => x.Result).Aggregate((x, y) => x & y);
        }

        /// <summary>
        /// Closes this all lines.
        /// </summary>
        public void Close()
        {
            Task[] closeTasks;
            lock (protocols)
            {
                closeTasks = new Task[protocols.Count];
                for (int i = 0; i < protocols.Count; i++)
                {
                    if (protocols[i].State == Line.Status.Ok)
                    {
                        var protocol = protocols[i];
                        closeTasks[i] = Task.Run(() => protocol.Protocol.Close());
                    }
                }
            }

            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Verbose, Resources.AttemptingToCloseProtocols, string.Concat(protocols.Select(x => string.Format(CultureInfo.CurrentCulture, "{0};", x.Protocol.DeviceInfo.LocationId))));

            for (int i = 0; i < closeTasks.Length; i++)
            {
                try
                {
                    closeTasks[i]?.Wait();
                }
                catch (AggregateException e)
                {
                    Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Error, Resources.ErrorClosingProtocolException, protocols[i].Protocol.DeviceInfo.LocationId, e);
                }
            }
        }

        /// <summary>
        /// Starts to listen with all lines. <see cref="LineProtocol.Listen"/>
        /// </summary>
        /// <exception cref="System.IO.IOException">Failed to listen with protocol.</exception>
        public void Listen()
        {
            Task[] listenTasks;
            lock (protocols)
            {
                listenTasks = new Task[protocols.Count];
                for (int i = 0; i < protocols.Count; i++)
                {
                    if (protocols[i].State == Line.Status.Ok)
                    {
                        var protocol = protocols[i];
                        listenTasks[i] = Task.Run(() => protocol.Protocol.Listen());
                    }
                }
            }

            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Information, Resources.AttemptingToListenWithProtocols, string.Concat(protocols.Select(x => string.Format(CultureInfo.CurrentCulture, "{0};", x.Protocol.DeviceInfo.LocationId))));

            for (int i = 0; i < listenTasks.Length; i++)
            {
                try
                {
                    listenTasks[i]?.Wait();
                }
                catch (AggregateException e)
                {
                    Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Error, Resources.ErrorListeningWithProtocolException, protocols[i].Protocol.DeviceInfo.LocationId, e);

                    throw new IOException("Failed to listen with protocol " + protocols[i].Protocol.DeviceInfo.LocationId + ".", e);
                }
            }
        }

        /// <summary>
        /// Writes the data to all lines.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <exception cref="System.IO.IOException">Line is closing.</exception>
        public void Write(byte[] buffer, int offset, int count)
        {
            ThrowIfFailed();
            if (Status == LineProtocol.LineStatus.Closing || Status == LineProtocol.LineStatus.Closed)
            {
                throw new IOException("Line is closing.");
            }

            List<byte[]> toSend = Serialize(buffer, offset, count);
            
            Task[] writeTasks;
            lock (protocols)
            {
                writeTasks = new Task[protocols.Count];
                for (int i = 0; i < protocols.Count; i++)
                {
                    if (protocols[i].State != Line.Status.Recovering)
                    {
                        var protocol = protocols[i];
                        writeTasks[i] = Task.Run(() =>
                        {
                            for (int j = 0; j < toSend.Count; j++)
                            {
                                protocol.Protocol.Write(toSend[j], 0, toSend[j].Length);
                            }
                        });
                    }
                    else
                    {
                        Recover(protocols[i]);
                    }
                }
            }

            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Verbose, Resources.StartingToWriteBOnProtocols, count, string.Concat(protocols.Select(x => string.Format(CultureInfo.CurrentCulture, "{0};", x.Protocol.DeviceInfo.LocationId))));

            for (int i = 0; i < writeTasks.Length; i++)
            {
                try
                {
                    writeTasks[i]?.Wait();
                }
                catch (AggregateException e)
                {
                    Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Error, Resources.ErrorWrittingWithProtocol, protocols[i].Protocol.DeviceInfo.LocationId, e);

                    Recover(protocols[i]);
                }
            }
        }

        /// <summary>
        /// Reads data from all lines.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <returns>Number of bytes read.</returns>
        /// <exception cref="System.IO.IOException">Line is closing.</exception>
        public int Read(byte[] buffer, int offset, int count)
        {
            ThrowIfFailed();
            if (Status == LineProtocol.LineStatus.Closing || Status == LineProtocol.LineStatus.Closed)
            {
                throw new IOException("Line is closing.");
            }

            Task<int>[] readTasks;
            List<byte[]> buffers = new List<byte[]>();
            lock (protocols)
            {
                readTasks = new Task<int>[protocols.Count];
                for (int i = 0; i < protocols.Count; i++)
                {
                    if (protocols[i].State != Line.Status.Recovering)
                    {
                        var protocol = protocols[i];
                        buffers.Add(new byte[count]);
                        var buf = buffers[i];
                        readTasks[i] = Task.Run(() => protocol.Protocol.Read(buf, 0, count));
                    }
                    else
                    {
                        buffers.Add(null);
                        Recover(protocols[i]);
                    }
                }
            }

            for (int i = 0; i < readTasks.Length; i++)
            {
                try
                {
                    readTasks[i]?.Wait();
                }
                catch (AggregateException e)
                {
                    Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Error, Resources.ErrorReadingWithProtocols, protocols[i].Protocol.DeviceInfo.LocationId, e);

                    Recover(protocols[i]);
                }
            }

            // need full message before adding to buffer or need to know how many bytes are left - ie till we get next header
            // strip header and add to buffer
            // take note of seq#
            for (int i = 0; i < protocols.Count; i++)
            {
                if (readTasks[i] != null && readTasks[i].Status == TaskStatus.RanToCompletion)
                {
                    lock (protocols[i].Buffer)
                    {
                        protocols[i].Buffer.AddRange(buffers[i].Take(readTasks[i].Result).ToArray());
                    }
                }
            }

            // deserialize all available packets
            for (int i = 0; i < protocols.Count; i++)
            {
                if (protocols[i].State != Line.Status.Recovering)
                {
                    while (CanDeserialize(protocols[i].Buffer))
                    {
                        protocols[i].Received.Add(Deserialize(protocols[i].Buffer));
                    }
                }
            }

            CheckForMessageLag();
            SyncRecoveredLines();

            return ReadBuffers(buffer, offset, count);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    recoveryTasksCTS.Cancel();

                    foreach (var line in protocols)
                    {
                        line.RecoveryTask?.Wait();
                        line.Protocol.Dispose();
                    }

                    recoveryTasksCTS.Dispose();
                }

                disposedValue = true;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "Necessary to ensure recovery doesn't fail.")]
        private static void RecoverTask(Line line, CancellationToken cancelToken)
        {
            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Information, Resources.RecoveringProtocol, line.Protocol.DeviceInfo.LocationId);

            line.Buffer.Clear();
            line.Received.Clear();

            while (true)
            {
                if (cancelToken.IsCancellationRequested)
                {
                    return;
                }

                try
                {
                    line.Protocol.Dispose();
                }
                catch (Exception)
                {
                }

                while (cancelToken.IsCancellationRequested == false)
                {
                    // is device connected?
                    DeviceFactory.Default.RefreshDeviceInfo();
                    var dis = DeviceFactory.Default.DeviceInfo.ToList();
                    DeviceInfo di = null;
                    for (int i = 0; i < dis.Count(); i++)
                    {
                        if (dis.ElementAt(i).LocationId == line.Protocol.DeviceInfo.LocationId)
                        {
                            di = dis.ElementAt(i);
                        }
                    }

                    if (di == null)
                    {
                        Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Verbose, Resources.DeviceAppearsToBeDisconnectedWillRetry, line.Protocol.DeviceInfo.LocationId);
                        Task.Delay(100).Wait();
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                
                line.Protocol = new LineProtocol(line.Protocol.DeviceInfo);
                line.Protocol.Close();

                try
                {
                    for (int i = 0; i < 5; i++)
                    {
                        if (line.Protocol.Open(1000) == false)
                        {
                            if (cancelToken.IsCancellationRequested)
                            {
                                line.Protocol.Dispose();
                                return;
                            }

                            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Verbose, Resources.AttemptingToOpenProtocolWillRetry, line.Protocol.DeviceInfo.LocationId);
                        }
                        else
                        {
                            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Information, Resources.ProtocolIsOpenWillAttemptToSyncOnNextRead, line.Protocol.DeviceInfo.LocationId);
                            line.State = Line.Status.Syncing;
                            line.RecoveryTask = null;
                            return;
                        }
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        private static Packet Deserialize(List<byte> buffer)
        {
            int len = Utils.Int32From2ByteArray(buffer, 0);

            lock (buffer)
            {
                if (len + 4 > buffer.Count)
                {
                    return null;
                }

                int seq = Utils.Int32From2ByteArray(buffer, 2);
                
                var data = buffer.GetRange(4, len);
                buffer.RemoveRange(0, len + 4);
                
                return new Packet(seq, data.ToArray());
            }
        }

        private static bool CanDeserialize(IEnumerable<byte> buffer)
        {
            if (buffer.Count() < 2)
            {
                return false;
            }

            int len = Utils.Int32From2ByteArray(buffer, 0);

            if (len + 4 > buffer.Count())
            {
                return false;
            }

            return true;
        }

        private void CheckForMessageLag()
        {
            int maxAllowedLag = 5;

            for (int i = 0; i < protocols.Count; i++)
            {
                if (protocols[i].State == Line.Status.Ok && protocols[i].Received.Count != 0 && protocols[i].Received.LastOrDefault().SequenceNumber + maxAllowedLag < nextSequenceNumberReceive)
                {
                    Recover(protocols[i]);
                }
            }
        }

        private void SyncRecoveredLines()
        {
            for (int i = 0; i < protocols.Count; i++)
            {
                if (protocols[i].State == Line.Status.Syncing)
                {
                    if (SyncLine(protocols[i]))
                    {
                        lock (protocols)
                        {
                            protocols[i].State = Line.Status.Ok;
                        }
                    }
                }
            }
        }

        private bool SyncLine(Line line)
        {
            while (line.Received.Count > 0)
            {
                if (line.Received.First().SequenceNumber < nextSequenceNumberReceive)
                {
                    line.Received.RemoveAt(0);
                }
                else
                {
                    break;
                }
            }

            if (line.Received.Count == 0)
            {
                return false;
            }

            if (line.Received.First().SequenceNumber >= nextSequenceNumberReceive)
            {
                lock (protocols)
                {
                    line.State = Line.Status.Ok;
                    return true;
                }
            }

            return false;
        }

        private int ReadBuffers(byte[] buffer, int offset, int count)
        {
            ThrowIfFailed();
            if (Status == LineProtocol.LineStatus.Closing || Status == LineProtocol.LineStatus.Closed)
            {
                throw new IOException("Line is closing.");
            }

            do
            {
                if (readBuffer.Count > count)
                {
                    break;
                }
            }
            while (ReadMessageToBuffer());

            int ret;
            if (readBuffer.Count > count)
            {
                readBuffer.CopyTo(0, buffer, offset, count);
                readBuffer.RemoveRange(0, count);
                ret = count;
            }
            else
            {
                readBuffer.CopyTo(0, buffer, offset, readBuffer.Count);
                ret = readBuffer.Count;
                readBuffer.Clear();
            }

            Logger.Instance.Log(Logger.Id.ProtocolBundle, Logger.Level.Verbose, Resources.ReadBWithProtocols, ret, string.Concat(protocols.Select(x => string.Format(CultureInfo.CurrentCulture, "{0};", x.Protocol.DeviceInfo.LocationId))));
            return ret;
        }

        private bool ReadMessageToBuffer()
        {
            // process message
            List<Packet> packets = new List<Packet>();
            for (int i = 0; i < protocols.Count; i++)
            {
                if (protocols[i].State == Line.Status.Ok)
                {
                    while (true)
                    {
                        int? firstSeq = protocols[i].Received.FirstOrDefault()?.SequenceNumber;

                        if (firstSeq.HasValue)
                        {
                            if (firstSeq.Value < nextSequenceNumberReceive)
                            {
                                protocols[i].Received.RemoveAt(0);
                                continue;
                            }
                            else if (firstSeq.Value == nextSequenceNumberReceive)
                            {
                                packets.Add(protocols[i].Received.First());
                                protocols[i].Received.RemoveAt(0);
                            }
                        }

                        break;
                    }
                }
            }

            // here we have list of packets
            if (packets.Count == 0)
            {
                return false;
            }

            nextSequenceNumberReceive++;

            int index = 0;
            while (true)
            {
                List<byte> values = new List<byte>();
                for (int i = 0; i < packets.Count; i++)
                {
                    if (packets[i].Length > index)
                    {
                        values.Add(packets[i].Data[index]);
                    }
                }

                if (values.Count == 0)
                {
                    break;
                }

                byte value = values.GroupBy(x => x).Select(x => new { Value = x.Key, Count = x.Count() }).OrderByDescending(x => x.Count).First().Value;
                readBuffer.Add(value);

                index++;
            }

            return true;
        }

        private void Recover(Line line)
        {
            if (Status == LineProtocol.LineStatus.Closed || Status == LineProtocol.LineStatus.Closing)
            {
                return;
            }

            lock (protocols)
            {
                if (line.State == Line.Status.Recovering && line.RecoveryTask != null && line.RecoveryTask.IsFaulted == false)
                {
                    return;
                }

                line.State = Line.Status.Recovering;
            }

            line.RecoveryTask = Task.Run(() => RecoverTask(line, recoveryTasksCTS.Token), recoveryTasksCTS.Token);
        }
        
        private void IncNextSeq()
        {
            nextSequenceNumberSend = (nextSequenceNumberSend + 1) % 65536;
        }

        private List<byte[]> Serialize(byte[] data, int offset, int count)
        {
            List<byte[]> ret = new List<byte[]>();

            int currentOffset = offset;
            while (currentOffset < offset + count)
            {
                byte[] packet;
                if (count + offset - currentOffset > maxDataLength)
                {
                    packet = new byte[maxDataLength + 4];
                }
                else
                {
                    packet = new byte[count + offset - currentOffset + 4];
                }

                Utils.Int32To2ByteArray(packet.Length - 4, packet, 0);
                Utils.Int32To2ByteArray(nextSequenceNumberSend, packet, 2);
                IncNextSeq();
                Array.Copy(data, currentOffset, packet, 4, packet.Length - 4);
                currentOffset += packet.Length - 4;
                ret.Add(packet);
            }

            return ret;
        }
        
        private void ThrowIfFailed()
        {
            if (protocols.Where(x => x.State == Line.Status.Ok).Count() == 0)
            {
                throw new IOException("All lines failed.");
            }
        }
        
        private class Packet
        {
            public Packet(int seq, byte[] data)
            {
                SequenceNumber = seq;
                Data = data ?? throw new ArgumentNullException("data");
            }

            public int SequenceNumber { get; private set; }

            public int Length
            {
                get
                {
                    return Data.Length;
                }
            }

            public byte[] Data { get; private set; }
        }

        private class Line
        {
            public Line(LineProtocol protocol)
            {
                Protocol = protocol;
            }

            public enum Status
            {
                /// <summary>
                /// Ok status.
                /// </summary>
                Ok,

                /// <summary>
                /// Recovering status.
                /// </summary>
                Recovering,

                /// <summary>
                /// Syncing Status.
                /// </summary>
                Syncing
            }

            public LineProtocol Protocol { get; set; }

            public Status State { get; set; } = Status.Ok;

            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Set is used during the initial assignment.")]
            public List<byte> Buffer { get; private set; } = new List<byte>();

            // TOOD: linked list
            [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Set is used during the initial assignment.")]

            public List<Packet> Received { get; private set; } = new List<Packet>();

            // public int? CurrentPacketDataIndex { get; private set; }
            public Task RecoveryTask { get; set; }
        }
    }
}
