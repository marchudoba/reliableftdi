﻿namespace FtdiWrapper.Protocols
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using FtdiWrapper.Exceptions;
    using FtdiWrapper.Logging;
    using FtdiWrapper.Properties;

    /// <summary>
    /// This class represents a protocol used to communicate between two ftd232r devices.
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class LineProtocol : IDisposable
    {
        private readonly int maxRetransmitAttempts = 3;

        // TODO: allow to set this
        private readonly int frameSize = 1024;
        private readonly FrameSerializer serializer = new FrameSerializer();
        private readonly List<byte> receiveQueue = new List<byte>();
        private readonly CancellationTokenSource cancelReceiveTask = new CancellationTokenSource();

        // private readonly object sendLock = new object();
        private readonly AutoResetEvent ackReceivedEvent = new AutoResetEvent(false);
        private readonly ConcurrentQueue<ProtocolFrame> lastAcks = new ConcurrentQueue<ProtocolFrame>();
        private readonly ManualResetEvent openReceivedEvent = new ManualResetEvent(false);

        private DeviceStream deviceStream;
        private int nextSequenceNumber = 0;
        private int expectedSequenceNumber;
        private Task receiveTask;
        private bool isDisposed = false; // To detect redundant calls

        /// <summary>
        /// Initializes a new instance of the <see cref="LineProtocol"/> class.
        /// </summary>
        /// <param name="deviceInfo">The device information.</param>
        public LineProtocol(DeviceInfo deviceInfo)
        {
            this.DeviceInfo = deviceInfo;
            deviceStream = new DeviceStream(deviceInfo);
        }

        /// <summary>
        /// Availavle line states.
        /// </summary>
        public enum LineStatus
        {
            /// <summary>
            /// Closed state.
            /// </summary>
            Closed,

            /// <summary>
            /// Listening state.
            /// </summary>
            Listening,

            /// <summary>
            /// Opening state.
            /// </summary>
            Opening,

            /// <summary>
            /// Open state.
            /// </summary>
            Open,

            /// <summary>
            /// Closing state.
            /// </summary>
            Closing,

            /// <summary>
            /// Faulted state.
            /// </summary>
            Faulted
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public LineStatus Status { get; private set; } = LineStatus.Closed;

        /// <summary>
        /// Gets the device information.
        /// </summary>
        /// <value>
        /// The device information.
        /// </value>
        internal DeviceInfo DeviceInfo { get; }

        /// <summary>
        /// Protocol starts passively waiting for incoming requests for open.
        /// </summary>
        /// <exception cref="System.IO.IOException">Protocol is faulted.</exception>
        public void Listen()
        {
            ThrowIfDisposed();

            if (Status == LineStatus.Faulted)
            {
                throw new IOException("Protocol is faulted.");
            }
            else if (Status == LineStatus.Closed)
            {
                deviceStream.Open();
                deviceStream.Flush();
                receiveTask = Task.Factory.StartNew(() => ReceiveLoop(cancelReceiveTask.Token), cancelReceiveTask.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);
            }
            else
            {
                return;
            }

            InitSequenceNumber();
            
            ChangeStatus(LineStatus.Listening, "Listen called.");
        }

        /// <summary>
        /// Protocol tries to open the line.
        /// </summary>
        /// <param name="maxWaitTime">Defines the maximum time (int ms) for the request to succed.</param>
        /// <returns>True if the line open; false otherwise.</returns>
        /// <exception cref="System.IO.IOException">Protocol is faulted.</exception>
        public bool Open(int maxWaitTime)
        {
            ThrowIfDisposed();

            if (Status == LineStatus.Open)
            {
                return true;
            }

            if (Status == LineStatus.Faulted)
            {
                throw new IOException("Protocol is faulted.");
            }
            else if (Status == LineStatus.Closed)
            {
                deviceStream.Open();
                deviceStream.Flush();
                receiveTask = Task.Factory.StartNew(() => ReceiveLoop(cancelReceiveTask.Token), cancelReceiveTask.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);

                ChangeStatus(LineStatus.Opening, "Open called.");
            }

            InitSequenceNumber();

            SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.Open, GetNextSequenceNumber(ref nextSequenceNumber)));
            
            if (openReceivedEvent.WaitOne(maxWaitTime))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Closes the line.
        /// Do not call this method directly use <see cref="Dispose"/> instead.
        /// </summary>
        public void Close()
        {
            ThrowIfDisposed();

            if (Status == LineStatus.Closed || Status == LineStatus.Closing)
            {
                return;
            }
            else if (Status == LineStatus.Faulted || Status == LineStatus.Listening)
            {
                SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.Rst, GetNextSequenceNumber(ref nextSequenceNumber)));
            }

            try
            {
                ChangeStatus(LineStatus.Closing, "Close called.");

                Send(new ProtocolFrame(ProtocolFrame.FrameType.Close, GetNextSequenceNumber(ref nextSequenceNumber)));
            }
            catch (IOException)
            {
                SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.Rst, GetNextSequenceNumber(ref nextSequenceNumber)));
            }

            ChangeStatus(LineStatus.Closed, "Closed in close call.");
        }

        /// <summary>
        /// Writes the specified buffer into the device.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <exception cref="System.ArgumentNullException"><paramref name="buffer"/> is null</exception>
        /// <exception cref="System.ArgumentException">
        /// Offset is negative.
        /// or
        /// Count is non positive.
        /// or
        /// Offset + count greater than lenght of data array.
        /// </exception>
        public void Write(byte[] buffer, int offset, int count)
        {
            ThrowIfDisposed();
            ThrowIfNotOpen();
            CheckReceiveTaskHealth();

            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentException("Offset is negative.");
            }

            if (count < 1)
            {
                throw new ArgumentException("Count is non positive.");
            }

            if (offset + count > buffer.Length)
            {
                throw new ArgumentException("Offset + count greater than lenght of data array.");
            }

            List<byte> toSend = new List<byte>(buffer).GetRange(offset, count);

            List<byte[]> frameContents = new List<byte[]>();
            int contentOffset = 0;

            while (toSend.Count - contentOffset > frameSize - serializer.DataHeaderLength)
            {
                frameContents.Add(toSend.GetRange(contentOffset, frameSize - serializer.DataHeaderLength).ToArray());
                contentOffset += frameSize - serializer.DataHeaderLength;
            }

            // TODO: check what happens when nothing is left
            frameContents.Add(toSend.GetRange(contentOffset, toSend.Count - contentOffset).ToArray());
            
            for (int i = 0; i < frameContents.Count; i++)
            {
                var frame = new ProtocolFrame(ProtocolFrame.FrameType.Data, GetNextSequenceNumber(ref nextSequenceNumber), frameContents[i]);
                Send(frame);
                Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Verbose, Resources.ProtocolSeqWrittenB, DeviceInfo.LocationId, frame.SequenceNumber, frameContents[i].Length);
            }
        }

        /// <summary>
        /// Read data from the device tp the specified buffer.
        /// </summary>
        /// <param name="buffer">The buffer.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <returns>Number of bytes read to the buffer.</returns>
        /// <exception cref="System.ArgumentNullException"><paramref name="buffer"/> is null.</exception>
        /// <exception cref="System.ArgumentException">
        /// Offset is negative.
        /// or
        /// Count is non positive.
        /// or
        /// Offset + count greater than length of data array.
        /// </exception>
        public int Read(byte[] buffer, int offset, int count)
        {
            ThrowIfDisposed();
            ThrowIfNotOpen();
            CheckReceiveTaskHealth();

            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentException("Offset is negative.");
            }

            if (count < 1)
            {
                throw new ArgumentException("Count is non positive.");
            }

            if (offset + count > buffer.Length)
            {
                throw new ArgumentException("Offset + count greater than length of data array.");
            }

            lock (receiveQueue)
            {
                if (receiveQueue.Count > count)
                {
                    receiveQueue.CopyTo(0, buffer, offset, count);
                    receiveQueue.RemoveRange(0, count);

                    return count;
                }
                else
                {
                    receiveQueue.CopyTo(0, buffer, offset, receiveQueue.Count);

                    var ret = receiveQueue.Count;
                    receiveQueue.Clear();

                    return ret;
                }
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    try
                    {
                        Close();
                    }
                    catch (FTDIException)
                    {
                    }
                    catch (IOException)
                    {
                    }

                    cancelReceiveTask.Cancel();
                    receiveTask?.Wait();

                    cancelReceiveTask.Dispose();
                    ackReceivedEvent.Dispose();
                    openReceivedEvent.Dispose();
                    
                    deviceStream.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                isDisposed = true;
            }
        }

        private static int GetNextSequenceNumber(ref int seq)
        {
            seq = (seq + 1) % 65536;
            return seq;
        }

        private void Send(ProtocolFrame frame)
        {
            var toSend = serializer.Serialize(frame);

            for (int i = 0; i < maxRetransmitAttempts; i++)
            {
                SendNoAck(toSend);

                // sometimes we dont wait appropriate amount if time when resending causing line to be stuck resending
                bool signaled = ackReceivedEvent.WaitOne(500);

                if (CheckAck(frame))
                {
                    return;
                }
                else if (signaled)
                {
                    ackReceivedEvent.WaitOne(500);
                }

                Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Verbose, Resources.ProtocolRetransmittingFrameFrameSeq, DeviceInfo.LocationId, frame.SequenceNumber);
            }

            ChangeStatus(LineStatus.Faulted, "Didnt received ack in time.");
            throw new IOException("Didn't received ack in time.");
        }

        private void SendNoAck(byte[] frame)
        {
            // lock (sendLock)
            // {
            try
            {
                deviceStream.Write(frame, 0, frame.Length);
            }
            catch (IOException e)
            {
                ChangeStatus(LineStatus.Faulted, "Write failed.");
                throw new IOException("Underlying stream failed.", e);
            }

            // }
        }

        private void SendNoAck(ProtocolFrame frame)
        {
            var toSend = serializer.Serialize(frame);

            // lock (sendLock)
            // {
            try
            {
                deviceStream.Write(toSend, 0, toSend.Length);
            }
            catch (IOException e)
            {
                ChangeStatus(LineStatus.Faulted, "Write failed.");
                throw new IOException("Underlying stream failed.", e);
            }

            // }
        }

        private bool CheckAck(ProtocolFrame frame)
        {
            while (true)
            {
                ProtocolFrame ack;
                if (lastAcks.TryDequeue(out ack) == false)
                {
                    Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Verbose, Resources.ProtocolNoAckForFrameSeqReasonAckEmpty, DeviceInfo.LocationId, frame.SequenceNumber);
                    return false;
                }

                if (frame.SequenceNumber == ack.SequenceNumber)
                {
                    return true;
                }
                else
                {
                }
            }
        }
        
        private void ReceiveLoop(CancellationToken cancelToken)
        {
            List<byte> received = new List<byte>();
            int bufferSize = 1024;
            byte[] buffer = new byte[bufferSize];

            using (var waitHandle = deviceStream.RegisterForEvents(Native.Ftd2XX.Events.RXChar))
            {
                while (true)
                {
                    if (cancelToken.IsCancellationRequested)
                    {
                        return;
                    }

                    waitHandle.WaitOne(100);
                    waitHandle.Reset();

                    int read;
                    do
                    {
                        try
                        {
                            read = deviceStream.Read(buffer, 0, bufferSize);

                            received.AddRange(buffer.Take(read));
                        }
                        catch (IOException e)
                        {
                            Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Error, e);
                            ChangeStatus(LineStatus.Faulted, "Receive loop error.");
                            return;
                        }
                    }
                    while (read == bufferSize);

                    while (serializer.CanDeserialize(received))
                    {
                        ProtocolFrame frame = serializer.Deserialize(received);
                        ProcessMessage(frame);
                    }
                }
            }
        }

        private void ProcessMessage(ProtocolFrame frame)
        {
            switch (frame.Type)
            {
                case ProtocolFrame.FrameType.Data:
                    ProcessData(frame);
                    break;
                case ProtocolFrame.FrameType.Ack:
                    ProcessAck(frame);
                    break;
                case ProtocolFrame.FrameType.Close:
                    ProcessClose(frame);
                    break;
                case ProtocolFrame.FrameType.Open:
                    ProcessOpen(frame);
                    break;
                case ProtocolFrame.FrameType.Rst:
                    ProcessRst();
                    break;
                case ProtocolFrame.FrameType.OpenOk:
                    ProcessOpenOk(frame);
                    break;
            }
        }

        private void ProcessData(ProtocolFrame frame)
        {
            if (Status == LineStatus.Open)
            {
                // expected path
                if (frame.SequenceNumber == expectedSequenceNumber)
                {
                    SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.Ack, frame.SequenceNumber));
                    GetNextSequenceNumber(ref expectedSequenceNumber);
                    
                    lock (receiveQueue)
                    {
                        receiveQueue.AddRange(frame.Content);
                    }

                    Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Verbose, Resources.ProtocolSeqReadB, DeviceInfo.LocationId, frame.SequenceNumber, frame.Content.Length);
                }                
                else if (frame.SequenceNumber > expectedSequenceNumber)
                {
                    // missed some data -> faulted
                    ChangeStatus(LineStatus.Faulted, "Received out of sequence frame.");
                }                
                else
                {
                    // happens when resending
                    SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.Ack, frame.SequenceNumber));
                }
            }
        }

        private void ProcessAck(ProtocolFrame frame)
        {
            if (Status == LineStatus.Listening)
            {
                if (frame.SequenceNumber == nextSequenceNumber)
                {
                    Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Verbose, Resources.ProtocolOpenNextSeqExpectedSeq, DeviceInfo.LocationId, nextSequenceNumber, expectedSequenceNumber);
                    ChangeStatus(LineStatus.Open, "Received ack while listening.");
                }

                return;
            }

            lastAcks.Enqueue(frame);
            ackReceivedEvent.Set();
        }

        private void ProcessClose(ProtocolFrame frame)
        {
            SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.Ack, frame.SequenceNumber));
            ChangeStatus(LineStatus.Closed, "Received close message.");
        }

        private void ProcessOpen(ProtocolFrame frame)
        {
            if (Status == LineStatus.Listening)
            {
                SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.OpenOk, GetNextSequenceNumber(ref nextSequenceNumber)));

                expectedSequenceNumber = frame.SequenceNumber + 1;
            }
            else if (Status == LineStatus.Opening)
            {
                expectedSequenceNumber = frame.SequenceNumber + 1;
                SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.OpenOk, GetNextSequenceNumber(ref nextSequenceNumber)));

                openReceivedEvent.Set();
                Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Verbose, Resources.ProtocolOpenNextSeqExpectedSeq, DeviceInfo.LocationId, nextSequenceNumber, expectedSequenceNumber);
                ChangeStatus(LineStatus.Open, "Received open while opening.");
            }
            else if (Status == LineStatus.Open)
            {
                expectedSequenceNumber = frame.SequenceNumber + 1;
                SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.OpenOk, GetNextSequenceNumber(ref nextSequenceNumber)));
            }
        }

        private void ProcessOpenOk(ProtocolFrame frame)
        {
            if (Status == LineStatus.Opening)
            {
                expectedSequenceNumber = frame.SequenceNumber + 1;
                ChangeStatus(LineStatus.Open, "Received open ok while opening.");
            }

            // we are open now
            SendNoAck(new ProtocolFrame(ProtocolFrame.FrameType.Ack, frame.SequenceNumber));
        }

        private void ProcessRst()
        {
            ChangeStatus(LineStatus.Closed, "Received rst.");
        }

        private void ChangeStatus(LineStatus newStatus, string reason)
        {
            if (Status == newStatus)
            {
                return;
            }

            Logger.Instance.Log(Logger.Id.Protocol, Logger.Level.Information, Resources.ProtocolChangingStatusFromToReason, DeviceInfo.LocationId, Status, newStatus, reason);
            switch (newStatus)
            {
                case LineStatus.Faulted:
                    cancelReceiveTask.Cancel();
                    break;
            }

            Status = newStatus;
        }

        private void InitSequenceNumber()
        {
            Random rnd = new Random();

            // there are two bytes for sequence# in the header
            nextSequenceNumber = rnd.Next(65535);
        }
        
        private void CheckReceiveTaskHealth()
        {
            if (receiveTask.IsFaulted)
            {
                receiveTask = Task.Factory.StartNew(() => ReceiveLoop(cancelReceiveTask.Token), cancelReceiveTask.Token, TaskCreationOptions.LongRunning, TaskScheduler.Current);
            }
        }
        
        private void ThrowIfNotOpen()
        {
            if (Status != LineStatus.Open)
            {
                throw new IOException("Line not open.");
            }
        }

        private void ThrowIfDisposed()
        {
            if (isDisposed)
            {
                throw new ObjectDisposedException("Line protocol");
            }
        }
    }
}
