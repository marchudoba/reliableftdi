﻿namespace FtdiWrapper.Protocols
{
    using System;

    /// <summary>
    /// This class represents a <see cref="LineProtocol"/> frame.
    /// </summary>
    internal class ProtocolFrame
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProtocolFrame"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="content">The content.</param>
        /// <exception cref="System.ArgumentException">
        /// Cannot create data frame with no content.
        /// or
        /// Cannot create control frame with content.
        /// </exception>
        public ProtocolFrame(FrameType type, int sequenceNumber, byte[] content = null)
        {
            if (type == FrameType.Data)
            {
                if (content == null || content.Length == 0)
                {
                    throw new ArgumentException("Cannot create data frame with no content.");
                }
            }
            else if (type != FrameType.Data && type != FrameType.None)
            {
                if (content != null)
                {
                    throw new ArgumentException("Cannot create control frame with content.");
                }
            }

            Type = type;
            SequenceNumber = sequenceNumber;
            Content = content;
        }

        /// <summary>
        /// Represents a frame type.
        /// </summary>
        public enum FrameType : byte
        {
            /// <summary>
            /// Represents empty frame type.
            /// </summary>
            None = 0,

            /// <summary>
            /// Open type.
            /// </summary>
            Open,

            /// <summary>
            /// OpenOk type.
            /// </summary>
            OpenOk,

            /// <summary>
            /// Ack type.
            /// </summary>
            Ack,

            /// <summary>
            /// Data type.
            /// </summary>
            Data,

            /// <summary>
            /// Close type.
            /// </summary>
            Close,

            /// <summary>
            /// Rst type.
            /// </summary>
            Rst
        }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public FrameType Type { get; protected set; }

        /// <summary>
        /// Gets or sets the sequence number.
        /// </summary>
        /// <value>
        /// The sequence number.
        /// </value>
        public int SequenceNumber { get; protected set; }

        /// <summary>
        /// Gets the content.
        /// </summary>
        /// <value>
        /// The content.
        /// </value>
        public byte[] Content { get; private set; }
    }
}
