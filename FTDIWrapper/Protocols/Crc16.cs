﻿namespace FtdiWrapper.Protocols
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class provides methods to calculate CRC 16 CCITT
    /// </summary>
    /// <remarks>
    /// Initial value is 0xFFFF.
    /// </remarks>
    public sealed class Crc16
    {
        private static ushort polynomial = 0x1021;
        private static ushort[] table = new ushort[256];
        private static bool initialized = false;

        private Crc16()
        {
        }

        /// <summary>
        /// Gets the checksum.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <returns>
        /// CRC of <paramref name="data" />
        /// </returns>
        /// <exception cref="ArgumentNullException">Thrown when data is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException">
        /// Thrown when offset is negative
        /// or
        /// count is not positive.
        /// </exception>
        /// <exception cref="ArgumentException">Sum of offset and count is larger then the length of the data.</exception>
        /// <exception cref="System.ArgumentNullException">Thrown when data is null.</exception>
        public static int GetChecksum(IEnumerable<byte> data, int offset, int count)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }

            if (offset < 0)
            {
                throw new ArgumentOutOfRangeException("offset");
            }

            if (count <= 0)
            {
                throw new ArgumentOutOfRangeException("count");
            }

            if (offset + count > data.Count())
            {
                throw new ArgumentException("Sum of offset and count is larger then the length of the data.");
            }
            
            BuildTable();

            ushort crc = 0xffff;
            for (int i = offset; i < offset + count; ++i)
            {
                crc = (ushort)((crc << 8) ^ table[((crc >> 8) ^ (0xff & data.ElementAt(i)))]);
            }

            return crc;
        }

        /// <summary>
        /// Gets the checksum bytes.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="offset">The offset.</param>
        /// <param name="count">The count.</param>
        /// <returns>
        /// CRC of <paramref name="data" />
        /// </returns>
        public static byte[] GetChecksumBytes(IEnumerable<byte> data, int offset, int count)
        {
            int crc = GetChecksum(data, offset, count);
            byte[] ret = new byte[2];
            ret[0] = (byte)(crc >> 8);
            ret[1] = (byte)crc;
            return ret;
        }

        private static void BuildTable()
        {
            if (initialized)
            {
                return;
            }

            ushort temp, a;
            for (int i = 0; i < table.Length; i++)
            {
                temp = 0;
                a = (ushort)(i << 8);
                for (int j = 0; j < 8; ++j)
                {
                    if (((temp ^ a) & 0x8000) != 0)
                    {
                        temp = (ushort)((temp << 1) ^ polynomial);
                    }
                    else
                    {
                        temp <<= 1;
                    }

                    a <<= 1;
                }

                table[i] = temp;
            }
        }
    }
}
