﻿namespace FtdiWrapper
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Management;
    using System.Text;
    using System.Threading.Tasks;
    using FtdiWrapper.Native;
    using Logging;

    /// <summary>
    /// Factory for creating <see cref="DeviceInfo"/>.
    /// </summary>
    public class DeviceFactory
    {
        private static object defaultConstructionLock = new object();
        private static DeviceFactory defaultInstace;
        private readonly Ftd2XX deviceDriver;

        private readonly List<DeviceInfo> deviceInfo = new List<DeviceInfo>();

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceFactory" /> class.
        /// </summary>
        /// <param name="deviceDriver">The device driver.</param>
        public DeviceFactory(Ftd2XX deviceDriver)
        {
            this.deviceDriver = deviceDriver ?? throw new ArgumentNullException("deviceDriver");

            DeviceFilter.Add(DeviceType.Device232R);
        }

        /// <summary>
        /// Gets the default instance which uses <see cref="NativeMethods"/>.
        /// </summary>
        /// <value>
        /// The default instance using <see cref="NativeMethods"/>.
        /// </value>
        public static DeviceFactory Default
        {
            get
            {
                if (defaultInstace == null)
                {
                    lock (defaultConstructionLock)
                    {
                        if (defaultInstace == null)
                        {
                            defaultInstace = new DeviceFactory(new NativeMethods());
                        }
                    }
                }

                return defaultInstace;
            }
        }

        /// <summary>
        /// Gets the device filter.
        /// </summary>
        /// <value>
        /// The device filter.
        /// </value>
        /// <remarks>
        /// Only <see cref="DeviceType"/> in this collection will be returned (default value is <see cref="DeviceType.Device232R"/>).
        /// Clear the collection to show all devices.
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Justification = "Analysis doesnt recognize call to set.")]
        public IList<DeviceType> DeviceFilter { get; private set; } = new List<DeviceType>();

        /// <summary>
        /// Gets the device information. Use <see cref="RefreshDeviceInfo"/> to update this list.
        /// </summary>
        /// <value>
        /// The device information.
        /// </value>
        public IEnumerable<DeviceInfo> DeviceInfo
        {
            get
            {
                lock (deviceInfo)
                {
                    return deviceInfo.Where(x => DeviceFilter.Contains(x.DeviceType)).ToList();
                }
            }
        }

        /// <summary>
        /// Recreates <see cref="DeviceInfo"/> based on <see cref="Ftd2XX.GetDeviceInfoList(uint)"/>.
        /// </summary>
        public void RefreshDeviceInfo()
        {
            lock (deviceInfo)
            {
                deviceInfo.Clear();

                uint numberOfDevices = deviceDriver.CreateDeviceInfoList();
                if (numberOfDevices > 0)
                {
                    FT_DEVICE_LIST_INFO_NODE[] nodes = deviceDriver.GetDeviceInfoList(numberOfDevices);

                    foreach (var item in nodes)
                    {
                        deviceInfo.Add(new DeviceInfo(item, deviceDriver));
                    }
                }
            }
        }
    }
}
