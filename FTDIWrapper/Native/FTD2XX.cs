﻿namespace FtdiWrapper.Native
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents all possible operations that can be done on the device.
    /// </summary>
    /// <autogeneratedoc />
    public abstract class Ftd2XX
    {
        /// <summary>
        /// Event types for <see cref=""/>
        /// </summary>
        [Flags]
        public enum Events
        {
            /// <summary>
            /// The event will be set when a character has been received
            /// by the device.
            /// </summary>
            RXChar = 1,

            /// <summary>
            /// The event will be set when a change in the modem
            /// signals has been detected by the device.
            /// </summary>
            ModemStatus = 2,

            /// <summary>
            /// The event will be set when a change in the line status
            /// has been detected by the device.
            /// </summary>
            LineStatus = 4
        }

        /// <summary>
        /// Determines word length.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Justification = "WordLength has to be byte to work properly with P/Invoke.")]
        public enum WordLength : byte
        {
            /// <summary>
            /// Represents invalid value.
            /// </summary>
            Invalid = 0,

            /// <summary>
            /// 8 bits per word.
            /// </summary>
            Bits8 = 8,

            /// <summary>
            /// 7 bits per word (MSB will be 0).
            /// </summary>
            /// <autogeneratedoc />
            Bits7 = 7
        }

        /// <summary>
        /// Determines number of stop bits.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Justification = "StopBits has to be byte to work properly with P/Invoke.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1717:OnlyFlagsEnumsShouldHavePluralNames", Justification = "Name is taken from FTD2XX.dll, it will reamain plural to avoid confusion.")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1027:MarkEnumsWithFlags", Justification = "StopBits are not defined as flags. Numbering is taken from FTD2XX.dll.")]
        public enum StopBits : byte
        {
            /// <summary>
            /// Indicates one bit is used.
            /// </summary>
            Bits1 = 0,

            /// <summary>
            /// Indicates two bits are used.
            /// </summary>
            Bits2 = 2
        }

        /// <summary>
        /// Determines parity.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1028:EnumStorageShouldBeInt32", Justification = "Parity has to be byte to work properly with P/Invoke.")]
        public enum Parity : byte
        {
            /// <summary>
            /// Indicates no parity is used.
            /// </summary>
            None = 0,

            /// <summary>
            /// Indicates odd parity is used.
            /// </summary>
            Odd = 1,

            /// <summary>
            /// Indicates even parity is used.
            /// </summary>
            Even = 2,

            /// <summary>
            /// Indicates mark parity is used.
            /// </summary>
            Mark = 3,

            /// <summary>
            /// Indicates space parity is used.
            /// </summary>
            Space = 4
        }

        /// <summary>
        /// Determines buffer.
        /// </summary>
        [Flags]
        public enum Buffers
        {
            /// <summary>
            /// The none
            /// </summary>
            None = 0,

            /// <summary>
            /// The receive buffer
            /// </summary>
            ReceiveBuffer = 1,

            /// <summary>
            /// The transmit buffer
            /// </summary>
            TransmitBuffer = 2
        }

        /// <summary>
        /// Creates the device information list.
        /// </summary>
        /// <returns>Size of the list.</returns>
        internal abstract uint CreateDeviceInfoList();

        /// <summary>
        /// Gets the device information list.
        /// </summary>
        /// <param name="numberOfDevices">The number of devices.</param>
        /// <returns>Array of device infos.</returns>
        internal abstract FT_DEVICE_LIST_INFO_NODE[] GetDeviceInfoList(uint numberOfDevices);

        /// <summary>
        /// Gets the device information detail.
        /// </summary>
        /// <param name="deviceListIndex">Index to the device list.</param>
        /// <param name="flags">The flags.</param>
        /// <param name="type">The type.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="locationId">The location identifier.</param>
        /// <param name="serialNumber">The serial number.</param>
        /// <param name="description">The description.</param>
        /// <param name="handle">The handle.</param>
        /// <autogeneratedoc />
        internal abstract void GetDeviceInfoDetail(
                                                                uint deviceListIndex,
                                                                ref FT_DEVICE_LIST_INFO_NODE.FLAGS flags,
                                                                ref FT_DEVICE_TYPE type,
                                                                ref uint id,
                                                                ref uint locationId,
                                                                byte[] serialNumber,
                                                                byte[] description,
                                                                ref IntPtr handle);

        /// <summary>
        /// Opens the device specified by index to <see cref="GetDeviceInfoList(uint)"/>.
        /// </summary>
        /// <param name="deviceIndex">Index of the device.</param>
        /// <returns>Handle to the device.</returns>
        internal abstract IntPtr Open(int deviceIndex);

        /// <summary>
        /// Opens the device by description.
        /// </summary>
        /// <param name="description">The description.</param>
        /// <returns>Handle to the device.</returns>
        internal abstract IntPtr OpenByDescription(string description);

        /// <summary>
        /// Opens the device by serial number.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <returns>Handle to the device.</returns>
        internal abstract IntPtr OpenBySerialNumber(string serialNumber);

        /// <summary>
        /// Opens the device by location.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>Handle to the device.</returns>
        internal abstract IntPtr OpenByLocation(uint location);

        /// <summary>
        /// Closes the device specified by <paramref name="handle"/>.
        /// </summary>
        /// <param name="handle">The handle.</param>
        internal abstract void Close(IntPtr handle);

        /// <summary>
        /// Writes data from the <paramref name="buffer"/> to the device specified by the <paramref name="handle"/>.
        /// </summary>
        /// <param name="handle">The handle to the device.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="bytesToWrite">The number of bytes to write.</param>
        /// <returns>Number of bytes written.</returns>
        internal abstract uint Write(IntPtr handle, byte[] buffer, uint bytesToWrite);

        /// <summary>
        /// Sets the baud rate.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <param name="baudRate">The baud rate.</param>
        /// <autogeneratedoc />
        internal abstract void SetBaudRate(IntPtr handle, uint baudRate);

        /// <summary>
        /// Sets the data characteristics.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <param name="wordLength">Number of bits per word .</param>
        /// <param name="stopBits">Number of stop bits.</param>
        /// <param name="parity">The parity.</param>
        /// <autogeneratedoc />
        internal abstract void SetDataCharacteristics(IntPtr handle, WordLength wordLength, StopBits stopBits, Parity parity);

        /// <summary>
        /// Reads the data from the device specified by the handle.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="bytesToRead">The bytes to read.</param>
        /// <returns>Number of bytes read.</returns>
        internal abstract uint Read(IntPtr handle, byte[] buffer, uint bytesToRead);

        /// <summary>
        /// Gets number of bytes in receive queue.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <returns>Number of bytes in receive queue.</returns>
        internal abstract uint GetQueueStatus(IntPtr handle);

        /// <summary>
        /// Sets the event notification.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <param name="events">The events.</param>
        /// <param name="eventHandle">The event handle.</param>
        internal abstract void SetEventNotification(IntPtr handle, Events events, EventWaitHandle eventHandle);

        /// <summary>
        /// Purges buffers specified in <paramref name="buffer"/>.
        /// </summary>
        /// <param name="handle">The handle.</param>
        /// <param name="buffer">The buffers.</param>
        internal abstract void Purge(IntPtr handle, Buffers buffer);
    }
}
