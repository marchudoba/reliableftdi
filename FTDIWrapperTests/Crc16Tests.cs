﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FtdiWrapper.Protocols;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class Crc16Tests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetChecksum_ThrowsWhenDataNull()
        {
            Crc16.GetChecksum(null, 0, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetChecksum_ThrowsWhenOffsetIsNegative()
        {
            Crc16.GetChecksum(new byte[1], -1, 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void GetChecksum_ThrowsWhenCountIsNotPositive()
        {
            Crc16.GetChecksum(new byte[1], 0, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void GetChecksum_ThrowsWhenSumOfOffsetAndCountIsLargerThenDataLength()
        {
            Crc16.GetChecksum(new byte[2], 1, 2);
        }

        [TestMethod]
        public void GetChecksum_CalculateCrc()
        {
            // expected taken from http://www.sunshine2k.de/coding/javascript/crc/crc_js.html with CRC16_CCITT_FALSE

            byte[] data = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int expected = 0x3B0A;
            int actual = Crc16.GetChecksum(data, 0, data.Length);

            Assert.AreEqual(expected, actual);

            data = new byte[] { 0 };
            expected = 0xE1F0;
            actual = Crc16.GetChecksum(data, 0, data.Length);

            Assert.AreEqual(expected, actual);

            data = new byte[] { 0xFF };
            expected = 0xFF00;
            actual = Crc16.GetChecksum(data, 0, data.Length);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void GetChecksumBytes_CalculateCrc()
        {
            byte[] data = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            byte[] expected = new byte[] { 0x3B,0x0A };
            byte[] actual = Crc16.GetChecksumBytes(data, 0, data.Length);

            CollectionAssert.AreEqual(expected, actual);

            data = new byte[] { 0 };
            expected = new byte[] { 0xE1,0xF0 };
            actual = Crc16.GetChecksumBytes(data, 0, data.Length);

            CollectionAssert.AreEqual(expected, actual);

            data = new byte[] { 0xFF };
            expected = new byte[] { 0xFF,0x00 };
            actual = Crc16.GetChecksumBytes(data, 0, data.Length);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
