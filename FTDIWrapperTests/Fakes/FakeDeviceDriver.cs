﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FtdiWrapper.Exceptions;
using FtdiWrapper.Native;

namespace FTDIWrapper.Tests.Fakes
{
    class FakeDeviceDriver : FtdiWrapper.Native.Ftd2XX
    {


        private int nextHandle = 1;

        public FakeDevice[] devices;

        internal FakeDeviceDriver(FT_DEVICE_LIST_INFO_NODE[] nodes)
        {
            devices = new FakeDevice[nodes.Length];
            for (int i = 0; i < devices.Length; i++)
            {
                devices[i] = new FakeDevice(nodes[i]);
            }
        }

        internal override void Close(IntPtr handle)
        {
            var nodeInfo = devices.Where(x => x.Handle == handle).First();
            nodeInfo.Handle = IntPtr.Zero;
            nodeInfo.InputStream.Clear();

        }

        internal override uint CreateDeviceInfoList()
        {
            foreach (var nodeInfo in devices.Where(x => x.Visible == true))
            {
                nodeInfo.CreateDeviceInfoListCalled = true;
            }

            return (uint)devices.Where(x => x.CreateDeviceInfoListCalled == true).Count();
        }

        internal override void GetDeviceInfoDetail(uint deviceListIndex, ref FT_DEVICE_LIST_INFO_NODE.FLAGS flags, ref FT_DEVICE_TYPE type, ref uint id, ref uint locationId, byte[] serialNumber, byte[] description, ref IntPtr handle)
        {
            var nodeInfo = devices[deviceListIndex];

            flags = nodeInfo.Node.Flags;
            type = nodeInfo.Node.Type;
            id = nodeInfo.Node.Id;
            locationId = nodeInfo.Node.LocationId;
            serialNumber = nodeInfo.Node.SerialNumber;
            description = nodeInfo.Node.Description;
            handle = nodeInfo.Node.Handle;
        }

        internal override FT_DEVICE_LIST_INFO_NODE[] GetDeviceInfoList(uint numberOfDevices)
        {
            FT_DEVICE_LIST_INFO_NODE[] nodes = new FT_DEVICE_LIST_INFO_NODE[numberOfDevices];

            for (int i = 0; i < devices.Length; i++)
            {
                nodes[i] = devices[i].Node;
            }

            return nodes;
        }

        internal override uint GetQueueStatus(IntPtr handle)
        {
            FakeDevice nodeInfo;
            try
            {
                nodeInfo = devices.Where(x => x.Handle == handle).First();
            }
            catch (Exception)
            {
                throw new FTDIException();
            }
            return (uint)nodeInfo.InputStream.Count;
        }

        internal override IntPtr Open(int deviceIndex)
        {
            var deviceInfo = devices[deviceIndex];
            deviceInfo.Handle = new IntPtr(nextHandle++);

            return deviceInfo.Handle;
        }

        internal override IntPtr OpenByDescription(string description)
        {
            throw new NotImplementedException();
        }

        internal override IntPtr OpenByLocation(uint location)
        {
            for (int i = 0; i < devices.Length; i++)
            {
                if (devices[i].Node.LocationId == location)
                {
                    return Open(i);
                }
            }
            throw new Exception();
        }

        internal override IntPtr OpenBySerialNumber(string serialNumber)
        {
            throw new NotImplementedException();
        }

        internal override uint Read(IntPtr handle, byte[] buffer, uint bytesToRead)
        {
            var nodeInfo = devices.Where(x => x.Handle == handle).First();

            return (uint)nodeInfo.Read(buffer, bytesToRead);
        }

        internal override void SetBaudRate(IntPtr handle, uint baudRate)
        {

        }

        internal override void SetDataCharacteristics(IntPtr handle, WordLength wordLength, StopBits stopBits, Parity parity)
        {
        }

        internal override void SetEventNotification(IntPtr handle, Events events, EventWaitHandle eventHandle)
        {
            var nodeInfo = devices.Where(x => x.Handle == handle).First();

            nodeInfo.WaitHandles.Add(new Tuple<EventWaitHandle, Events>(eventHandle, events));
        }

        internal override uint Write(IntPtr handle, byte[] buffer, uint bytesToWrite)
        {
            FakeDevice nodeInfo;
            try
            {
                nodeInfo = devices.Where(x => x.Handle == handle).First();
            }
            catch (Exception)
            {
                throw new FTDIException();
            }
            nodeInfo.Write(buffer, bytesToWrite);

            return bytesToWrite;
        }

        internal override void Purge(IntPtr handle, Buffers buffer)
        {
            var nodeInfo = devices.Where(x => x.Handle == handle).First();

            if (buffer.HasFlag(Buffers.ReceiveBuffer))
            {
                nodeInfo.InputStream.Clear();
            }
            if (buffer.HasFlag(Buffers.TransmitBuffer))
            {
                nodeInfo?.ConnectedTo?.InputStream?.Clear();
            }
        }

        public void ConnectDevices(int node1Index, int node2Index)
        {
            var device1 = devices[node1Index];
            var device2 = devices[node2Index];

            device1.ConnectedTo = device2;
            device2.ConnectedTo = device1;

        }
    }
}
