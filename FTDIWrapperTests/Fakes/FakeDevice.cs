﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FtdiWrapper.Native;
using static FtdiWrapper.Native.Ftd2XX;

namespace FTDIWrapper.Tests.Fakes
{
    class FakeDevice
    {
        public FakeDevice(FT_DEVICE_LIST_INFO_NODE node)
        {
            this.node = node;
            Handle = node.Handle;
        }

        private FT_DEVICE_LIST_INFO_NODE node;
        private FakeDevice connectedTo;

        public IntPtr Handle { get; set; }
        public List<byte> InputStream { get; set; } = new List<byte>();
        public FakeDevice ConnectedTo
        {
            get
            {
                return connectedTo;
            }
            set
            {
                connectedTo = value;
            }
        }
        public FT_DEVICE_LIST_INFO_NODE Node
        {
            get
            {
                FT_DEVICE_LIST_INFO_NODE ret = node;
                ret.Handle = Handle;
                ret.Flags |= Handle != IntPtr.Zero ? FT_DEVICE_LIST_INFO_NODE.FLAGS.FT_FLAGS_OPENED : FT_DEVICE_LIST_INFO_NODE.FLAGS.FT_FLAGS_NONE;
                return ret;
            }
        }
        public bool Visible { get; set; } = true;
        public bool CreateDeviceInfoListCalled { get; set; } = false;
        public List<Tuple<EventWaitHandle, Events>> WaitHandles { get; set; } = new List<Tuple<EventWaitHandle, Events>>();

        public object Param { get; set; }
        public int WriteDelay { get; set; }
        public int ReadDelay { get; set; }
        public Func<byte[], byte[]> BeforeWrite { get; set; }
        public Func<byte[], uint, uint> AfterRead { get; set; }

        public uint Write(byte[] buffer, uint bytesToWrite)
        {
            Thread.Sleep(WriteDelay);
            if (BeforeWrite != null)
            {
                buffer = BeforeWrite(buffer);
            }

            if (connectedTo != null)
            {
                if (buffer.Length != bytesToWrite)
                {
                    byte[] writeData = new byte[bytesToWrite];
                    Array.Copy(buffer, 0, writeData, 0, bytesToWrite);
                    connectedTo.InputStream.AddRange(writeData);
                }
                else
                {
                    connectedTo.InputStream.AddRange(buffer);
                }

                for (int i = 0; i < connectedTo.WaitHandles.Count; i++)
                {
                    if (connectedTo.WaitHandles[i].Item2 == Events.RXChar)
                    {
                        try
                        {
                            connectedTo.WaitHandles[i].Item1.Set();
                        }
                        catch (ObjectDisposedException)
                        {
                            connectedTo.WaitHandles.RemoveAt(i);
                            i--;
                        }
                    }
                }

                return bytesToWrite;
            }
            return 0;
        }

        public uint Read(byte[] buffer, uint bytesToRead)
        {
            int i;
            for (i = 0; i < bytesToRead; i++)
            {
                buffer[i] = InputStream[0];
                InputStream.RemoveAt(0);
            }

            Thread.Sleep(ReadDelay);
            if (AfterRead != null)
            {
                return AfterRead.Invoke(buffer, (uint)i);
            }
            return (uint)i;
        }
    }
}
