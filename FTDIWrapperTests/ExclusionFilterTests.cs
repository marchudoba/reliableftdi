﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FtdiWrapper.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class ExclusionFilterTests
    {
        [TestMethod]
        public void Match()
        {
            ExclusionFilter filter = new ExclusionFilter();

            filter.Add(Logger.Id.DeviceFactory);

            LogEntry entry = new LogEntry(Logger.Id.DeviceFactory, Logger.Level.Critical);

            Assert.AreEqual(true, filter.Match(entry));

            entry = new LogEntry(Logger.Id.DeviceStream, Logger.Level.Critical);

            Assert.AreEqual(false, filter.Match(entry));

            filter.Add(Logger.Level.Information);

            entry = new LogEntry(Logger.Id.DeviceStream, Logger.Level.Information);

            Assert.AreEqual(true, filter.Match(entry));
        }
    }
}
