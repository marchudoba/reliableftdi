﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FtdiWrapper.Protocols;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class FrameSerializerTests
    {
        [TestMethod]
        public void Deserialize_ControlFrame()
        {
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Ack;
            int expectedSequenceNumber = 1;

            byte[] buffer = new byte[5];
            buffer[0] = (byte)expectedFrameType;

            Utils.Int32To2ByteArray(expectedSequenceNumber, buffer, 1);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 3), buffer, 3);

            FrameSerializer ser = new FrameSerializer();
            ProtocolFrame frame = ser.Deserialize(buffer.ToList());

            Assert.AreEqual(expectedFrameType, frame.Type);
            Assert.AreEqual(expectedSequenceNumber, frame.SequenceNumber);
        }

        [TestMethod]
        public void Deserialize_DataFrame()
        {
            byte[] data = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 };
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Data;
            int expectedSequenceNumber = 1;

            byte[] buffer = new byte[9 + data.Length];

            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(expectedSequenceNumber, buffer, 1);
            Utils.Int32To2ByteArray(data.Length, buffer, 3);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 5), buffer, 5);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(data, 0, data.Length), buffer, 7);
            Array.Copy(data, 0, buffer, 9, data.Length);

            FrameSerializer ser = new FrameSerializer();
            ProtocolFrame frame = ser.Deserialize(buffer.ToList());

            Assert.AreEqual(expectedFrameType, frame.Type);
            Assert.AreEqual(expectedSequenceNumber, frame.SequenceNumber);
            CollectionAssert.AreEqual(data, frame.Content);
        }

        [TestMethod]
        public void CanDeserialize_ControlFrameNoErrors()
        {
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Ack;
            int expectedSequenceNumber = 1;

            byte[] buffer = new byte[5];
            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(expectedSequenceNumber, buffer, 1);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 3), buffer, 3);

            FrameSerializer ser = new FrameSerializer();
            Assert.AreEqual(true, ser.CanDeserialize(buffer.ToList()));
        }

        [TestMethod]
        public void CanDeserialize_ControlFrameShorterBuffer()
        {
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Ack;
            int expectedSequenceNumber = 1;

            byte[] buffer = new byte[4];
            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(expectedSequenceNumber, buffer, 1);

            FrameSerializer ser = new FrameSerializer();
            Assert.AreEqual(false, ser.CanDeserialize(buffer.ToList()));
        }

        [TestMethod]
        public void CanDeserialize_ControlFrameCrcError()
        {
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Ack;
            int expectedSequenceNumber = 1;

            byte[] buffer = new byte[5];
            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(expectedSequenceNumber, buffer, 1);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 3), buffer, 3);
            buffer[3] = 0x00;

            FrameSerializer ser = new FrameSerializer();
            Assert.AreEqual(false, ser.CanDeserialize(buffer.ToList()));
        }

        [TestMethod]
        public void CanDeserialize_DataFrameNoErrors()
        {
            byte[] data = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 };
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Data;

            byte[] buffer = new byte[9 + data.Length];

            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(1, buffer, 1);
            Utils.Int32To2ByteArray(data.Length, buffer, 3);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 5), buffer, 5);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(data, 0, data.Length), buffer, 7);
            Array.Copy(data, 0, buffer, 9, data.Length);

            FrameSerializer ser = new FrameSerializer();
            Assert.AreEqual(true, ser.CanDeserialize(buffer.ToList()));
        }

        [TestMethod]
        public void CanDeserialize_DataFrameShorterBuffer()
        {
            byte[] data = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 };
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Data;

            byte[] buffer = new byte[9 + data.Length - 1];

            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(1, buffer, 1);
            Utils.Int32To2ByteArray(data.Length, buffer, 3);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 5), buffer, 5);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(data, 0, data.Length), buffer, 7);
            Array.Copy(data, 0, buffer, 9, data.Length - 1);

            FrameSerializer ser = new FrameSerializer();
            Assert.AreEqual(false, ser.CanDeserialize(buffer.ToList()));
        }

        [TestMethod]
        public void CanDeserialize_HeaderCrcError()
        {
            byte[] data = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 };
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Data;

            byte[] buffer = new byte[9 + data.Length];

            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(1, buffer, 1);
            Utils.Int32To2ByteArray(data.Length, buffer, 3);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 5), buffer, 5);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(data, 0, data.Length), buffer, 7);
            Array.Copy(data, 0, buffer, 9, data.Length);

            buffer[5] = 0;

            FrameSerializer ser = new FrameSerializer();
            Assert.AreEqual(false, ser.CanDeserialize(buffer.ToList()));
        }

        [TestMethod]
        public void CanDeserialize_DataFrameDataCrcError()
        {
            byte[] data = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 };
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Data;

            byte[] buffer = new byte[9 + data.Length];

            buffer[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(1, buffer, 1);
            Utils.Int32To2ByteArray(data.Length, buffer, 3);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(buffer, 0, 5), buffer, 5);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(data, 0, data.Length), buffer, 7);
            Array.Copy(data, 0, buffer, 9, data.Length);

            buffer[7] = 0;

            FrameSerializer ser = new FrameSerializer();
            Assert.AreEqual(false, ser.CanDeserialize(buffer.ToList()));
        }

        [TestMethod]
        public void Serialize_ControlFrame()
        {
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Ack;
            int expectedSequenceNumber = 1;

            byte[] expected = new byte[5];
            expected[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(expectedSequenceNumber, expected, 1);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(expected, 0, 3), expected, 3);

            ProtocolFrame expectedFrame = new ProtocolFrame(expectedFrameType, expectedSequenceNumber);
            FrameSerializer ser = new FrameSerializer();
            var actual = ser.Serialize(expectedFrame);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Serialize_DataFrame()
        {
            byte[] data = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09 };
            ProtocolFrame.FrameType expectedFrameType = ProtocolFrame.FrameType.Data;
            int expectedSequenceNumber = 1;

            byte[] expected = new byte[9 + data.Length];

            expected[0] = (byte)expectedFrameType;
            Utils.Int32To2ByteArray(expectedSequenceNumber, expected, 1);
            Utils.Int32To2ByteArray(data.Length, expected, 3);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(expected, 0, 5), expected, 5);
            Utils.Int32To2ByteArray(Crc16.GetChecksum(data, 0, data.Length), expected, 7);
            Array.Copy(data, 0, expected, 9, data.Length);

            ProtocolFrame frame = new ProtocolFrame(expectedFrameType, expectedSequenceNumber, data);

            FrameSerializer ser = new FrameSerializer();
            var actual = ser.Serialize(frame);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
