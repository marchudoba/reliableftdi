﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FtdiWrapper;
using FtdiWrapper.Native;
using FtdiWrapper.Protocols;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class LineProtocolTests
    {
        [TestMethod]
        public void Open()
        {
            var help = MakeProtocols();

            using (var p1 = help.Protocols[0])
            using (var p2 = help.Protocols[1])
            {

                p1.Listen();
                p2.Open(1000);

                Thread.Sleep(1000);

                Assert.AreEqual(LineProtocol.LineStatus.Open, p1.Status);
                Assert.AreEqual(LineProtocol.LineStatus.Open, p2.Status);
            }
        }

        [TestMethod]
        public void Close()
        {
            var help = MakeProtocols();

            LineProtocol p1;
            LineProtocol p2;

            using (p1 = help.Protocols[0])
            using (p2 = help.Protocols[1])
            {

                p1.Listen();
                p2.Open(1000);

                Thread.Sleep(1000);

                Assert.AreEqual(LineProtocol.LineStatus.Open, p1.Status);
                Assert.AreEqual(LineProtocol.LineStatus.Open, p2.Status);
            }

            Assert.AreEqual(LineProtocol.LineStatus.Closed, p1.Status);
            Assert.AreEqual(LineProtocol.LineStatus.Closed, p2.Status);
        }

        [TestMethod]
        public void Write_1B()
        {
            var help = MakeProtocols();

            using (var p1 = help.Protocols[0])
            using (var p2 = help.Protocols[1])
            {

                p1.Listen();
                p2.Open(1000);

                Thread.Sleep(1000);

                Assert.AreEqual(LineProtocol.LineStatus.Open, p1.Status);
                Assert.AreEqual(LineProtocol.LineStatus.Open, p2.Status);

                byte[] toSend = new byte[1];
                toSend[0] = 0xFF;

                p1.Write(toSend, 0, 1);

                byte[] buffer = new byte[1];
                p2.Read(buffer, 0, 1);

                CollectionAssert.AreEqual(toSend, buffer);
            }
        }

        [TestMethod]
        public void Write_1015B()
        {
            var help = MakeProtocols();

            using (var p1 = help.Protocols[0])
            using (var p2 = help.Protocols[1])
            {

                p1.Listen();
                p2.Open(1000);

                Thread.Sleep(1000);

                Assert.AreEqual(LineProtocol.LineStatus.Open, p1.Status);
                Assert.AreEqual(LineProtocol.LineStatus.Open, p2.Status);

                Random rnd = new Random();
                byte[] toSend = new byte[1015];
                rnd.NextBytes(toSend);

                p1.Write(toSend, 0, toSend.Length);

                byte[] buffer = new byte[toSend.Length];

                Thread.Sleep(100);

                p2.Read(buffer, 0, toSend.Length);

                CollectionAssert.AreEqual(toSend, buffer);
            }
        }

        [TestMethod]
        public void Write_1024B()
        {
            var help = MakeProtocols();

            using (var p1 = help.Protocols[0])
            using (var p2 = help.Protocols[1])
            {

                p1.Listen();
                p2.Open(1000);

                Thread.Sleep(1000);

                Assert.AreEqual(LineProtocol.LineStatus.Open, p1.Status);
                Assert.AreEqual(LineProtocol.LineStatus.Open, p2.Status);

                Random rnd = new Random();
                byte[] toSend = new byte[1024];
                rnd.NextBytes(toSend);

                p1.Write(toSend, 0, toSend.Length);

                byte[] buffer = new byte[toSend.Length];
                p2.Read(buffer, 0, toSend.Length);

                CollectionAssert.AreEqual(toSend, buffer);
            }
        }

        private TestSetupHelper MakeProtocols()
        {
            FT_DEVICE_LIST_INFO_NODE node1 = new FT_DEVICE_LIST_INFO_NODE
            {
                Description = Encoding.ASCII.GetBytes("Description"),
                Flags = FT_DEVICE_LIST_INFO_NODE.FLAGS.FT_FLAGS_NONE,
                Handle = IntPtr.Zero,
                Id = 0xFFFFEEEE,
                LocationId = 1,
                SerialNumber = Encoding.ASCII.GetBytes("SerialNumber"),
                Type = FT_DEVICE_TYPE.FT_DEVICE_232H
            };

            FT_DEVICE_LIST_INFO_NODE node2 = new FT_DEVICE_LIST_INFO_NODE
            {
                Description = Encoding.ASCII.GetBytes("Description"),
                Flags = FT_DEVICE_LIST_INFO_NODE.FLAGS.FT_FLAGS_NONE,
                Handle = IntPtr.Zero,
                Id = 0xFFFFEEEE,
                LocationId = 2,
                SerialNumber = Encoding.ASCII.GetBytes("SerialNumber"),
                Type = FT_DEVICE_TYPE.FT_DEVICE_232H
            };

            var driver = new Fakes.FakeDeviceDriver(new FT_DEVICE_LIST_INFO_NODE[] { node1, node2 });
            driver.ConnectDevices(0, 1);


            DeviceInfo info1 = new DeviceInfo(node1, driver);
            DeviceInfo info2 = new DeviceInfo(node2, driver);

            TestSetupHelper ret = new TestSetupHelper();

            ret.Protocols.Add(new LineProtocol(info1));
            ret.Protocols.Add(new LineProtocol(info2));

            ret.DeviceInfos.Add(info1);
            ret.DeviceInfos.Add(info2);

            ret.Nodes.Add(node1);
            ret.Nodes.Add(node2);

            return ret;
        }

        class TestSetupHelper
        {
            public List<LineProtocol> Protocols { get; private set; } = new List<LineProtocol>();
            public List<FT_DEVICE_LIST_INFO_NODE> Nodes { get; private set; } = new List<FT_DEVICE_LIST_INFO_NODE>();
            public List<DeviceInfo> DeviceInfos { get; private set; } = new List<DeviceInfo>();
        }


    }
}
