﻿using System;
using System.IO;
using FakeItEasy;
using FtdiWrapper;
using FtdiWrapper.Exceptions;
using FtdiWrapper.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class DeviceStreamTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_ThrowsWhenDeviceInfoIsNull()
        {
            using (DeviceStream stream = new DeviceStream(null))
            {

            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Open_ThrowsWhenBaudrateOutOfRange()
        {
            IntPtr deviceHandle = new IntPtr(100);
            uint deviceLocation = 1;

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(deviceLocation)).Returns(deviceHandle);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();
            node.LocationId = deviceLocation;

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open(0);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void Open_ThrowsWhenDisposed()
        {

            var deviceDriver = A.Fake<Ftd2XX>();

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Dispose();
                stream.Open(100);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void Open_ThrowsWhenOpenFails()
        {
            IntPtr handle = new IntPtr(10);
            int baudRate = 100;

            var deviceDriver = A.Fake<Ftd2XX>();

            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Throws(new FTDIException());
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored)).Throws(new FTDIException());
            A.CallTo(() => deviceDriver.SetBaudRate(handle, (uint)baudRate));

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open(baudRate);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void Open_ThrowsWhenSetDataCharacteristicsFails()
        {
            IntPtr handle = new IntPtr(10);
            int baudRate = 100;

            var deviceDriver = A.Fake<Ftd2XX>();

            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored)).Throws(new FTDIException());
            A.CallTo(() => deviceDriver.SetBaudRate(handle, (uint)baudRate));

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();


            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open(baudRate);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void Open_ThrowsWhenSetBaudRateFails()
        {
            IntPtr handle = new IntPtr(10);
            int baudRate = 100;

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored)).Throws(new FTDIException());

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open(baudRate);
            }
        }

        [TestMethod]
        public void Open_DriverInteraction()
        {
            IntPtr deviceHandle = new IntPtr(100);
            uint deviceLocation = 1;

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(deviceLocation)).Returns(deviceHandle);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();
            node.LocationId = deviceLocation;

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open(100);

                A.CallTo(() => deviceDriver.OpenByLocation(deviceLocation)).MustHaveHappened();
                A.CallTo(() => deviceDriver.SetBaudRate(deviceHandle, 100)).MustHaveHappened();
                Assert.AreEqual(deviceHandle, deviceInfo.Handle);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Read_ThrowsWhenBufferIsNull()
        {
            IntPtr handle = new IntPtr(10);
            byte[] read = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.GetQueueStatus(handle)).Returns((uint)read.Length);
            // A.CallTo(() => deviceDriver.Read(handle, A<byte[]>.Ignored, (uint)read.Length)).Returns((uint)read.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                byte[] buffer = null;
                var bytesRead = stream.Read(buffer, 0, read.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Read_ThrowsWhenOffsetIsNegative()
        {
            IntPtr handle = new IntPtr(10);
            byte[] read = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.GetQueueStatus(handle)).Returns((uint)read.Length);
            A.CallTo(() => deviceDriver.Read(handle, A<byte[]>.Ignored, (uint)read.Length))
                .Invokes((IntPtr h, byte[] b, uint l) => Array.Copy(read, b, read.Length)).Returns((uint)read.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                byte[] buffer = new byte[read.Length];
                var bytesRead = stream.Read(buffer, -1, buffer.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Read_ThrowsWhenCountIsNotPositive()
        {
            IntPtr handle = new IntPtr(10);
            byte[] read = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.GetQueueStatus(handle)).Returns((uint)read.Length);
            A.CallTo(() => deviceDriver.Read(handle, A<byte[]>.Ignored, (uint)read.Length))
                .Invokes((IntPtr h, byte[] b, uint l) => Array.Copy(read, b, read.Length)).Returns((uint)read.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                byte[] buffer = new byte[read.Length];
                var bytesRead = stream.Read(buffer, 0, 0);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Read_ThrowsWhenSumOfOffsetAndCountIsGreaterThanBufferLength()
        {
            IntPtr handle = new IntPtr(10);
            byte[] read = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.GetQueueStatus(handle)).Returns((uint)read.Length);
            A.CallTo(() => deviceDriver.Read(handle, A<byte[]>.Ignored, (uint)read.Length))
                .Invokes((IntPtr h, byte[] b, uint l) => Array.Copy(read, b, read.Length)).Returns((uint)read.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                byte[] buffer = new byte[read.Length];
                var bytesRead = stream.Read(buffer, 1, buffer.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void Read_ThrowsWhenReadFails()
        {
            IntPtr handle = new IntPtr(10);

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.GetQueueStatus(handle)).Returns((uint)1);
            A.CallTo(() => deviceDriver.Read(handle, A<byte[]>.Ignored, A<uint>.Ignored)).Throws<FTDIException>();

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                byte[] buffer = new byte[1];
                var bytesRead = stream.Read(buffer, 0, buffer.Length);
            }
        }

        [TestMethod]
        public void Read_DriverInteraction()
        {
            IntPtr handle = new IntPtr(10);
            byte[] read = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.GetQueueStatus(handle)).Returns((uint)read.Length);
            A.CallTo(() => deviceDriver.Read(handle, A<byte[]>.Ignored, (uint)read.Length))
                .Invokes((IntPtr h, byte[] b, uint l) => Array.Copy(read, b, read.Length)).Returns((uint)read.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                byte[] buffer = new byte[read.Length];
                var bytesRead = stream.Read(buffer, 0, buffer.Length);

                Assert.AreEqual(read.Length, bytesRead);
                for (int i = 0; i < buffer.Length; i++)
                {
                    Assert.AreEqual(read[i], buffer[i]);
                }
            }
        }

        [TestMethod]
        public void Write_DriverInteraction()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).ReturnsLazily((IntPtr h, byte[] b, uint c) => (uint)b.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                stream.Write(write, 0, write.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void Write_ThrowsWriteFailed()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).Throws(new FTDIException());

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                stream.Write(write, 0, write.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Write_ThrowsWhenBufferIsNull()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).ReturnsLazily((IntPtr h, byte[] b, uint c) => (uint)b.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                stream.Write(null, 0, write.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Write_ThrowsWhenOffsetIsNegative()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).ReturnsLazily((IntPtr h, byte[] b, uint c) => (uint)b.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                stream.Write(write, -1, write.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Write_ThrowsWhenCountIsNotPositive()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).ReturnsLazily((IntPtr h, byte[] b, uint c) => (uint)b.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                stream.Write(write, 0, 0);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Write_ThrowsWhenSumOfOffsetAndCountIsGreaterThanBufferLength()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).ReturnsLazily((IntPtr h, byte[] b, uint c) => (uint)b.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            using (DeviceStream stream = new DeviceStream(deviceInfo))
            {
                stream.Open();

                stream.Write(write, 1, write.Length);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void Read_ThrowsWhenDisposed()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).ReturnsLazily((IntPtr h, byte[] b, uint c) => (uint)b.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            DeviceStream stream;
            using (stream = new DeviceStream(deviceInfo))
            {
            }
            stream.Read(null,0,0);
        }

        [TestMethod]
        [ExpectedException(typeof(ObjectDisposedException))]
        public void Write_ThrowsWhenDisposed()
        {
            IntPtr handle = new IntPtr(10);
            byte[] write = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };

            var deviceDriver = A.Fake<Ftd2XX>();
            A.CallTo(() => deviceDriver.OpenByLocation(A<uint>.Ignored)).Returns(handle);
            A.CallTo(() => deviceDriver.SetDataCharacteristics(A<IntPtr>.Ignored, A<Ftd2XX.WordLength>.Ignored, A<Ftd2XX.StopBits>.Ignored, A<Ftd2XX.Parity>.Ignored));
            A.CallTo(() => deviceDriver.SetBaudRate(A<IntPtr>.Ignored, A<uint>.Ignored));
            A.CallTo(() => deviceDriver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).ReturnsLazily((IntPtr h, byte[] b, uint c) => (uint)b.Length);

            FT_DEVICE_LIST_INFO_NODE node = new FT_DEVICE_LIST_INFO_NODE();

            DeviceInfo deviceInfo = new DeviceInfo(node, deviceDriver);
            DeviceStream stream;
            using (stream = new DeviceStream(deviceInfo))
            {
            }
            stream.Write(null, 0, 0);
        }
    }
}
