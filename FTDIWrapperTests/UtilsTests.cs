﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FtdiWrapper.Protocols;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class UtilsTests
    {
        [TestMethod]
        public void IntFromByteArray_IntFromArray()
        {
            byte[] data = { 0x2A, 0x2B, 0xCC, 0xDD, 0xEE };

            var actual = Utils.Int32FromByteArray(data, 0);

            Assert.AreEqual(707513565, actual);

            actual = Utils.Int32FromByteArray(data, 1);

            Assert.AreEqual(734846446, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IntFromByteArray_ThrowsWhenBufferIsNull()
        {
            byte[] data = null;

            var actual = Utils.Int32FromByteArray(data, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void IntFromByteArray_ThrowsWhenBufferTooSmall()
        {
            byte[] data = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE };

            var actual = Utils.Int32FromByteArray(data, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void IntFromByteArray_ThrowsWhenOffsetNegative()
        {
            byte[] data = { 0xAA, 0xBB, 0xCC, 0xDD, 0xEE };

            var actual = Utils.Int32FromByteArray(data, -1);
        }

        [TestMethod]
        public void IntToByteArray_IntToArray()
        {
            byte[] buffer = new byte[5];

            Utils.Int32ToByteArray(0x2ABBCCDD, buffer, 0);

            Assert.AreEqual(0x2A, buffer[0]);
            Assert.AreEqual(0xBB, buffer[1]);
            Assert.AreEqual(0xCC, buffer[2]);
            Assert.AreEqual(0xDD, buffer[3]);

            Utils.Int32ToByteArray(0x2ABBCCDD, buffer, 1);

            Assert.AreEqual(0x2A, buffer[1]);
            Assert.AreEqual(0xBB, buffer[2]);
            Assert.AreEqual(0xCC, buffer[3]);
            Assert.AreEqual(0xDD, buffer[4]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IntToByteArray_ThrowsWhenBufferIsNull()
        {
            byte[] buffer = null;

            Utils.Int32ToByteArray(0x2ABBCCDD, buffer, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void IntToByteArray_ThrowsWhenBufferTooSmall()
        {
            byte[] buffer = new byte[5];

            Utils.Int32ToByteArray(0x2ABBCCDD, buffer, 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void IntToByteArray_ThrowsWhenOffsetNegative()
        {
            byte[] buffer = new byte[5];

            Utils.Int32ToByteArray(0x2ABBCCDD, buffer, -2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void IntTo2ByteArray_ThrowsWhenValueTooHigh()
        {
            int value = 65537;
            byte[] actual = new byte[2];

            Utils.Int32To2ByteArray(value, actual, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IntTo2ByteArray_ThrowsWhenBufferIsNull()
        {
            int value = 6;

            Utils.Int32To2ByteArray(value, null, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Int32To2ByteArray_ThrowsWhenOffsetNegative()
        {
            int value = 65537;
            byte[] actual = new byte[2];

            Utils.Int32To2ByteArray(value, actual, -1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Int32To2ByteArray_ThrowsWhenBufferTooSmall()
        {
            int value = 65537;
            byte[] actual = new byte[2];

            Utils.Int32To2ByteArray(value, actual, 1);
        }

        [TestMethod]
        public void Int32To2ByteArray_IntToArray()
        {
            int value = 65535;
            byte[] actual = new byte[2];

            Utils.Int32To2ByteArray(value, actual, 0);
            CollectionAssert.AreEqual(new byte[] { 0xFF, 0xFF }, actual);

            value = 0xAABB;
            Utils.Int32To2ByteArray(value, actual, 0);
            CollectionAssert.AreEqual(new byte[] { 0xAA, 0xBB }, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Int32From2ByteArray_ThrowsWhenBufferIsNull()
        {
            Utils.Int32From2ByteArray(null, 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Int32From2ByteArray_ThrowsWhenBufferTooSmall()
        {
            byte[] buffer = new byte[2];

            Utils.Int32From2ByteArray(buffer, 1);
        }
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Int32From2ByteArray_ThrowsWhenOffsetIsNegative()
        {
            byte[] buffer = new byte[2];

            Utils.Int32From2ByteArray(buffer, -1);
        }

        [TestMethod]
        public void Int32From2ByteArray()
        {
            int actual;

            actual = Utils.Int32From2ByteArray(new byte[] { 0xFF, 0xFF}, 0);
            Assert.AreEqual(0xFFFF, actual);

            actual = Utils.Int32From2ByteArray(new byte[] { 0xAA, 0xBB }, 0);
            Assert.AreEqual(0xAABB, actual);
        }
    }
}
