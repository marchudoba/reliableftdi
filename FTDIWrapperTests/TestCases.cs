﻿using FakeItEasy;
using FtdiWrapper;
using FtdiWrapper.Native;
using FtdiWrapper.Protocols;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class TestCases
    {
        // these need be ordered so bundle1LocationIds[i] is connected to bundle2LocationIds[i]
        private int[] bundle1LocationIds = new int[] { 354, 353 };
        private int[] bundle2LocationIds = new int[] { 355, 356 };

        [TestMethod]
        public void TransmissionError()
        {
            var driver = A.Fake<NativeMethods>(options => options.CallsBaseMethods());
            var factory = new DeviceFactory(driver);


            using (var p1 = CreateProtocol(factory, bundle1LocationIds.First()))
            using (var p2 = CreateProtocol(factory, bundle2LocationIds.First()))
            {

                p1.Listen();

                while (p2.Open(1000) == false) { }

                Random rand = new Random();
                var toSend = new byte[100];
                rand.NextBytes(toSend);

                A.CallTo(() => driver.Write(A<IntPtr>.Ignored, A<byte[]>.Ignored, A<uint>.Ignored)).Invokes(x =>
                {
                    var b = x.GetArgument<byte[]>(1);
                    b[10] = 0x00;
                }).CallsBaseMethod().Once();

                p1.Write(toSend, 0, toSend.Length);

                byte[] buffer = new byte[100];
                int bRead = 0;
                while (bRead < 100)
                {
                    bRead += p2.Read(buffer, 0, buffer.Length);
                }

                CollectionAssert.AreEqual(toSend, buffer);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void NativeCallFreezes()
        {
            var driver = A.Fake<NativeMethods>(options => options.CallsBaseMethods());
            var factory = new DeviceFactory(driver);


            using (var p1 = CreateProtocol(factory, bundle1LocationIds.First()))
            using (var p2 = CreateProtocol(factory, bundle2LocationIds.First()))
            {
                p1.Listen();

                while (p2.Open(1000) == false) { }

                Random rand = new Random();
                var toSend = new byte[100];
                rand.NextBytes(toSend);
                p1.Write(toSend, 0, toSend.Length);
                    
                byte[] buffer = new byte[100];
                int bRead = 0;
                while (bRead < 100)
                {
                    bRead += p2.Read(buffer, 0, buffer.Length);
                }

                var p2Handle = p2.DeviceInfo.Handle;
                A.CallTo(() => driver.Read(A<IntPtr>.That.Matches((x)=>x==p2Handle), A<byte[]>.Ignored, A<uint>.Ignored)).Throws(new TimeoutException());

                toSend = new byte[100];
                rand.NextBytes(toSend);
                p1.Write(toSend, 0, toSend.Length);

                buffer = new byte[100];
                bRead = 0;
                while (bRead < 100)
                {
                    bRead += p2.Read(buffer, 0, buffer.Length);
                }
            }
        }

        private LineProtocol CreateProtocol(DeviceFactory factory, int locationId)
        {
            factory.RefreshDeviceInfo();
            var di = factory.DeviceInfo.Where(x => x.LocationId == locationId).FirstOrDefault();

            if (di == null)
            {
                throw new ArgumentException("Device not found. Connect the device or edit bundleLocationIds field.");
            }

            return new LineProtocol(di);
        }
        private ProtocolBundle CreateBundle(DeviceFactory factory, params int[] locationIds)
        {
            factory.RefreshDeviceInfo();
            var dis = factory.DeviceInfo;

            List<DeviceInfo> selectedIds = new List<DeviceInfo>();
            for (int i = 0; i < locationIds.Length; i++)
            {
                var di = dis.Where(x => x.LocationId == locationIds[i]).FirstOrDefault();
                if (di == null)
                {
                    return null;
                }
                else
                {
                    selectedIds.Add(di);
                }
            }

            if (selectedIds.Count != locationIds.Length)
            {
                throw new ArgumentException("Device not found. Connect the device or edit bundleLocationIds field.");
            }

            return new ProtocolBundle(selectedIds.ToArray());
        }
    }
}
