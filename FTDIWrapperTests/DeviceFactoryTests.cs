﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakeItEasy;
using FtdiWrapper;
using FtdiWrapper.Native;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FTDIWrapper.Tests
{
    [TestClass]
    public class DeviceFactoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_ThrowsWhenDriverIsNull()
        {
            DeviceFactory factory = new DeviceFactory(null);
        }

        [TestMethod]
        public void Constructor_Basic()
        {
            DeviceFactory factory = new DeviceFactory(new NativeMethods());
        }

        [TestMethod]
        public void RefreshDeviceInfo_Interactions()
        {
            FT_DEVICE_LIST_INFO_NODE[] nodes = new FT_DEVICE_LIST_INFO_NODE[2];
            nodes[0].Type = FT_DEVICE_TYPE.FT_DEVICE_232R;
            nodes[1].Type = FT_DEVICE_TYPE.FT_DEVICE_232R;

            var driver = A.Fake<Ftd2XX>();
            A.CallTo(() => driver.CreateDeviceInfoList()).Returns((uint)2);
            A.CallTo(() => driver.GetDeviceInfoList(2)).Returns(nodes);


            DeviceFactory factory = new DeviceFactory(driver);

            int deviceNumberBefore = factory.DeviceInfo.Count();

            factory.RefreshDeviceInfo();

            int deviceNumberAfter = factory.DeviceInfo.Count();

            Assert.AreEqual(0, deviceNumberBefore);
            Assert.AreEqual(2, deviceNumberAfter);
            A.CallTo(() => driver.CreateDeviceInfoList()).MustHaveHappened();
            A.CallTo(() => driver.GetDeviceInfoList(2)).MustHaveHappened();
        }
    }
}
